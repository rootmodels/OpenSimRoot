var classCarbonCostOfBiologicalNitrogenFixation =
[
    [ "CarbonCostOfBiologicalNitrogenFixation", "classCarbonCostOfBiologicalNitrogenFixation.html#a3b0d1de1e58279b8245cb39028942f42", null ],
    [ "calculate", "classCarbonCostOfBiologicalNitrogenFixation.html#a246f272d433d5205ffe9042861f0a9fa", null ],
    [ "getName", "classCarbonCostOfBiologicalNitrogenFixation.html#a88fe34d97d61f7891a02eada8707ceba", null ],
    [ "factor", "classCarbonCostOfBiologicalNitrogenFixation.html#a16a950f2f3a4a987dc5fc110fa9cc1ff", null ],
    [ "fixation", "classCarbonCostOfBiologicalNitrogenFixation.html#ab1166726c16a983de975a488529f33b1", null ]
];