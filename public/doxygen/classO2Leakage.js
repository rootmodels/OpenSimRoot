var classO2Leakage =
[
    [ "O2Leakage", "classO2Leakage.html#ab581ab6b82276bf15bfe9ace7c3f948b", null ],
    [ "calculate", "classO2Leakage.html#a4cebf317ec897a71d163ed6808199913", null ],
    [ "getName", "classO2Leakage.html#aa0ff39d3a95a8f7ee72cc9de306c9f0f", null ],
    [ "pLeafTemperature", "classO2Leakage.html#ad9207dadc4afe9a825b104053319fa62", null ],
    [ "pMesophyllO", "classO2Leakage.html#a5bd383c6cde428ddf230dc56b5e9a851", null ],
    [ "pSheathO", "classO2Leakage.html#a01363b093d89913f02c730bb84d54c1c", null ],
    [ "sheathConductanceActivationEnergy", "classO2Leakage.html#a680454bb2c99587ba56c25114bd438b4", null ],
    [ "sheathConductanceAt25C", "classO2Leakage.html#a842d59b67b679e03e00cc5403b5dfa7d", null ],
    [ "sheathConductanceDeactivationEnergy", "classO2Leakage.html#a2c1e0e298837991b72743295cf4944e7", null ],
    [ "sheathConductanceEntropyTerm", "classO2Leakage.html#a8d7dab1aa419bc5ac43c00ad85c0e972", null ]
];