var classWeighedAverageOfSunlitAndShaded =
[
    [ "WeighedAverageOfSunlitAndShaded", "classWeighedAverageOfSunlitAndShaded.html#a3771ccdf4d4699385b87b5b1ccfd4b3e", null ],
    [ "calculate", "classWeighedAverageOfSunlitAndShaded.html#a9112e2b3d6d88efc40fc1810adc022cd", null ],
    [ "getName", "classWeighedAverageOfSunlitAndShaded.html#a550be96c1dd4ff99728b88fb2d019ce4", null ],
    [ "shaded", "classWeighedAverageOfSunlitAndShaded.html#a6ad735aa9a71ceadda2a3bf59d2085d2", null ],
    [ "shadedLeafAreaIndex", "classWeighedAverageOfSunlitAndShaded.html#a1e65bd92733dbfcb2aeb92e124ac6be8", null ],
    [ "sunlit", "classWeighedAverageOfSunlitAndShaded.html#aa482b21566acd1272fdc06f8a1599922", null ],
    [ "sunlitLeafAreaIndex", "classWeighedAverageOfSunlitAndShaded.html#a57ba1cc29012b44584368293deb2a3d3", null ]
];