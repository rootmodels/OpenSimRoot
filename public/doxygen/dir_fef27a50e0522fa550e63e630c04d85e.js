var dir_fef27a50e0522fa550e63e630c04d85e =
[
    [ "DoussanModel.cpp", "DoussanModel_8cpp.html", "DoussanModel_8cpp" ],
    [ "DoussanModel.hpp", "DoussanModel_8hpp.html", [
      [ "LateralHydraulicConductivity", "classLateralHydraulicConductivity.html", "classLateralHydraulicConductivity" ],
      [ "WaterUptakeDoussanModel", "classWaterUptakeDoussanModel.html", "classWaterUptakeDoussanModel" ],
      [ "HydraulicConductivityRootSystem", "classHydraulicConductivityRootSystem.html", "classHydraulicConductivityRootSystem" ]
    ] ],
    [ "WaterUptakeByRoots.cpp", "WaterUptakeByRoots_8cpp.html", null ],
    [ "WaterUptakeByRoots.hpp", "WaterUptakeByRoots_8hpp.html", [
      [ "WaterUptakeFromHopmans", "classWaterUptakeFromHopmans.html", "classWaterUptakeFromHopmans" ],
      [ "ScaledWaterUptake", "classScaledWaterUptake.html", "classScaledWaterUptake" ],
      [ "GetValuesFromPlantWaterUptake", "classGetValuesFromPlantWaterUptake.html", "classGetValuesFromPlantWaterUptake" ]
    ] ]
];