var classRootDiameterImpedance =
[
    [ "RootDiameterImpedance", "classRootDiameterImpedance.html#a0c3144d63d629266e82e03755e86b0ed", null ],
    [ "calculate", "classRootDiameterImpedance.html#a4adc851f1c1dab86cab0f19b755bcf49", null ],
    [ "getName", "classRootDiameterImpedance.html#ababd485da1fd2cd05037bb2caa14b82e", null ],
    [ "diameterScalingExponent", "classRootDiameterImpedance.html#add57e3df82ec4e7c11e49a9948ed9e89", null ],
    [ "lengthImpedance", "classRootDiameterImpedance.html#ae9033b5d27cdbe59c2f613f8aae47e9a", null ]
];