var classMesh6 =
[
    [ "Mesh6", "classMesh6.html#a3ec2c88c52f49411c6b9fce1a89c501d", null ],
    [ "~Mesh6", "classMesh6.html#a975e1d3e03d0424daa0c65241a961259", null ],
    [ "calc_elem_corners", "classMesh6.html#a752f2c3f188fe1f36ca524a102f1dcd1", null ],
    [ "easy_init_matrix", "classMesh6.html#ad93e11b322f77e7932b6c7616365964b", null ],
    [ "easy_init_matrix", "classMesh6.html#a2638d92d2bc12fcdc794270b3e60c639", null ],
    [ "getNsubel", "classMesh6.html#a7e8800f9ed3c96580985560e3c8ad781", null ],
    [ "set_det_coefficients", "classMesh6.html#af5455d3c2a9226427ffe7a1b40df4018", null ],
    [ "setSubelements", "classMesh6.html#ac669e7484a65b6a343b5db1dac039518", null ]
];