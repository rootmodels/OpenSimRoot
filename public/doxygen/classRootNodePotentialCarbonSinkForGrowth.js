var classRootNodePotentialCarbonSinkForGrowth =
[
    [ "RootNodePotentialCarbonSinkForGrowth", "classRootNodePotentialCarbonSinkForGrowth.html#a6411bd95f84aa5e11549ed8707677d5e", null ],
    [ "calculate", "classRootNodePotentialCarbonSinkForGrowth.html#a1a4c35f8b0310d055441451b0b427763", null ],
    [ "getName", "classRootNodePotentialCarbonSinkForGrowth.html#a6869961fe5dbb99005f7158312b0215e", null ],
    [ "CinDryWeight", "classRootNodePotentialCarbonSinkForGrowth.html#a7edad80886843953d07ceff6b9166f8a", null ],
    [ "density", "classRootNodePotentialCarbonSinkForGrowth.html#a8d8cde01b4faa08b90c170b2553752e8", null ],
    [ "diameter", "classRootNodePotentialCarbonSinkForGrowth.html#af55a3353423f198b1fed1977abcc7cce", null ],
    [ "growth", "classRootNodePotentialCarbonSinkForGrowth.html#a8973fe0054ab4b775c73454f5d7f192a", null ],
    [ "surfaceArea", "classRootNodePotentialCarbonSinkForGrowth.html#adf3d3ae94c9e6488ebea45f5eddf7a13", null ]
];