var classRootDataPoints =
[
    [ "RootDataPoints", "classRootDataPoints.html#a60f168c74d6dc6784b544a580211101e", null ],
    [ "generate", "classRootDataPoints.html#a657e58f8bf1cfde4ce556979cf14564b", null ],
    [ "initialize", "classRootDataPoints.html#a04c9eb38597d76cc83063c915d2c5f9f", null ],
    [ "count", "classRootDataPoints.html#aab15477a216e0ed4af277908e1286f8d", null ],
    [ "growthpoint", "classRootDataPoints.html#a7699600e672fa6e42b4e6ffe9ab7a008", null ],
    [ "lastPosition", "classRootDataPoints.html#a58671a87f6e2646cac613d5c90ebd970", null ],
    [ "lastUpdated", "classRootDataPoints.html#a40d004d16d1b146e7716f5579ac14c78", null ],
    [ "length", "classRootDataPoints.html#a01295ba91ecb7d83757831e23ee2de51", null ],
    [ "timeLastPoint", "classRootDataPoints.html#a8d09d779c9cce801f77dda84718ef6b8", null ]
];