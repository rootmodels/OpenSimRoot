var classPEPCarboxylationRate =
[
    [ "PEPCarboxylationRate", "classPEPCarboxylationRate.html#ad2942b846782d87f0de44a909dfb475e", null ],
    [ "calculate", "classPEPCarboxylationRate.html#af26656b0d5a7cbd5800b6900224a49a0", null ],
    [ "getName", "classPEPCarboxylationRate.html#a380c65b266e24f994a9706c518f9ffcb", null ],
    [ "atmosphericCO2Concentration", "classPEPCarboxylationRate.html#ae1ee071c2945e6bfd84e4560d4d65a65", null ],
    [ "cachedLeafTemperature", "classPEPCarboxylationRate.html#adcdad754b42abdd86b6028d517207c44", null ],
    [ "cachedTime", "classPEPCarboxylationRate.html#a737ee8387e39a0cc60bf54eb5a5eb64a", null ],
    [ "michaelisPEP", "classPEPCarboxylationRate.html#a481598d96ea697386749a7841ea67dc6", null ],
    [ "michaelisPEPActivationEnergy", "classPEPCarboxylationRate.html#adad3735474000f4a73de33e8c64811d3", null ],
    [ "michaelisPEPAt25C", "classPEPCarboxylationRate.html#ab9a9050ce45b8f537d2d1c2b335332a3", null ],
    [ "pepCarboxylationActivationEnergy", "classPEPCarboxylationRate.html#ab42eb3f993ccc30eeff0cb7a6d8913d2", null ],
    [ "pepCarboxylationDeactivationEnergy", "classPEPCarboxylationRate.html#a6e37fd93ddd2f3eeb5c1db84bcb648da", null ],
    [ "pepCarboxylationEntropyTerm", "classPEPCarboxylationRate.html#a29fa884172c99429c6bc4e83292c7d90", null ],
    [ "pepRegeneration", "classPEPCarboxylationRate.html#a60fd5b7a7094d726f1f680309756f767", null ],
    [ "pLeafTemperature", "classPEPCarboxylationRate.html#a3e1bed554edcd635978227e841b33e43", null ],
    [ "pMaxPEPCarboxylationAt25C", "classPEPCarboxylationRate.html#a8fdced249891112f4c4fd91faf3fcd0d", null ],
    [ "pMesophyllC", "classPEPCarboxylationRate.html#a5a13ad1abd94f5fc14516138460270d5", null ],
    [ "pStomatalConductance", "classPEPCarboxylationRate.html#ab2e99e3c0d3dc336d443a73b998f40f2", null ],
    [ "referenceMaxPEPCarboxylation", "classPEPCarboxylationRate.html#aebf3f4fa3547d43c112f2910b914decd", null ],
    [ "temperatureScalingFactor", "classPEPCarboxylationRate.html#a05ff4d53fc6ac96d90de96d918a95f61", null ]
];