var classSunlitLeafIrradiation =
[
    [ "SunlitLeafIrradiation", "classSunlitLeafIrradiation.html#aa967c905596a69b65c34ff20e26698c5", null ],
    [ "calculate", "classSunlitLeafIrradiation.html#aa98aefd509c4e43365d9f7a8d30a8644", null ],
    [ "getName", "classSunlitLeafIrradiation.html#a6d622c5508c0af82c162f24fdfc701a8", null ],
    [ "cachedSunlitInterception", "classSunlitLeafIrradiation.html#a6e50433ea3202a522b01611fcf2da7f6", null ],
    [ "cachedTime", "classSunlitLeafIrradiation.html#ae9b3d9a46ca51c1cb3ece2e9d307b915", null ],
    [ "diffuseReflection", "classSunlitLeafIrradiation.html#a9e9f506113ffb7c86e5d0687e8dd236c", null ],
    [ "horizontalReflection", "classSunlitLeafIrradiation.html#adf20f0d4871cd45716d1ab368e208ba7", null ],
    [ "leafAbsorptance", "classSunlitLeafIrradiation.html#ace2cd322ca3967e893699b295cfda494", null ],
    [ "leafAbsorptanceTerm", "classSunlitLeafIrradiation.html#aaaa098366abdf03973f4acc2f2405447", null ],
    [ "pBeamIrradiation", "classSunlitLeafIrradiation.html#acad11c78f36f96bf872e6a29066ba214", null ],
    [ "pDiffuseIrradiation", "classSunlitLeafIrradiation.html#a0cf0b6fedda8f2e6ad3992066fed9e8d", null ],
    [ "pLeafAreaIndex", "classSunlitLeafIrradiation.html#aa64b7c91c01b82915ebadb774832aff1", null ],
    [ "pSolarElevationAngle", "classSunlitLeafIrradiation.html#ae4a0bb03348dddf85529bdc8dc631a08", null ],
    [ "pSunlitLeafAreaIndex", "classSunlitLeafIrradiation.html#aa5818b6fc9406e4e9bc5f8592fc71132", null ],
    [ "pTotalLeafIrradiation", "classSunlitLeafIrradiation.html#a0f1eaf80fc50c6b11dd7a989e76d2133", null ]
];