var classImpedanceWhalley =
[
    [ "ImpedanceWhalley", "classImpedanceWhalley.html#a4031ca92d20301c2781e9b9f1d353c02", null ],
    [ "calculate", "classImpedanceWhalley.html#a12b1a4b17b7c84ec51403f7f4d0887de", null ],
    [ "getName", "classImpedanceWhalley.html#a9e9d985dde3a99ea69669848a4f5fe45", null ],
    [ "bulkDensity", "classImpedanceWhalley.html#aab2e511ff2a475eda0866a2cee58924c", null ],
    [ "localWaterContent", "classImpedanceWhalley.html#a4cecb95f7e8a7293858251d5c0ab50ba", null ],
    [ "residualWaterContent", "classImpedanceWhalley.html#a5a9b1880b8286074e8f2d1da49730adb", null ],
    [ "saturatedWaterContent", "classImpedanceWhalley.html#ab83f8ffe302466619a4ef594cb214fc3", null ],
    [ "vanGenuchtenAlpha", "classImpedanceWhalley.html#ad89f4f0a6c6080a4718c86273fa47496", null ],
    [ "vanGenuchtenN", "classImpedanceWhalley.html#a921bd471676cad3c213cb5e97088bfcb", null ]
];