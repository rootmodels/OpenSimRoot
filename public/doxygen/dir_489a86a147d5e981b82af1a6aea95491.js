var dir_489a86a147d5e981b82af1a6aea95491 =
[
    [ "proximity.cpp", "proximity_8cpp.html", "proximity_8cpp" ],
    [ "proximity.hpp", "proximity_8hpp.html", [
      [ "Proximity", "classProximity.html", "classProximity" ]
    ] ],
    [ "RootLengthProfile.cpp", "RootLengthProfile_8cpp.html", null ],
    [ "RootLengthProfile.hpp", "RootLengthProfile_8hpp.html", [
      [ "RootLengthProfile", "classRootLengthProfile.html", "classRootLengthProfile" ],
      [ "RootPropertyDepthProfile", "classRootPropertyDepthProfile.html", "classRootPropertyDepthProfile" ],
      [ "RootLengthDensity", "classRootLengthDensity.html", "classRootLengthDensity" ],
      [ "SuperCoring", "classSuperCoring.html", "classSuperCoring" ],
      [ "D95", "classD95.html", "classD95" ],
      [ "RootsBelowD95Solute", "classRootsBelowD95Solute.html", "classRootsBelowD95Solute" ]
    ] ]
];