var classReservesSinkBased =
[
    [ "ReservesSinkBased", "classReservesSinkBased.html#aba2800a0e4aec36781f9ed70860abe52", null ],
    [ "calculate", "classReservesSinkBased.html#a6edcede7ae587d3e4f06a37f867c8fe1", null ],
    [ "getName", "classReservesSinkBased.html#ab2f0633ef4960ce0016f29c0529790e9", null ],
    [ "exudates", "classReservesSinkBased.html#a38cc261c499c32d6ba1386a25ea858a2", null ],
    [ "mr", "classReservesSinkBased.html#a6bf1d2f76c1e9293b6aa8eb1e3c145f6", null ],
    [ "mt", "classReservesSinkBased.html#ae83d5adcf0a0b4e692964578d9653cc0", null ],
    [ "photosynthesis", "classReservesSinkBased.html#afecc1bd667339204733f9b30b63f6bbc", null ],
    [ "respiration", "classReservesSinkBased.html#add107a7f7c758beb7a7786a98a84ef96", null ],
    [ "sink", "classReservesSinkBased.html#a6bdf5fa0d5d4ff9bab1d063e15e22e9b", null ]
];