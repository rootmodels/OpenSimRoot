var classRootImpedanceFromBulkDensity =
[
    [ "RootImpedanceFromBulkDensity", "classRootImpedanceFromBulkDensity.html#acb2aea1a86a8802fa8c7989e2bbdc570", null ],
    [ "calculate", "classRootImpedanceFromBulkDensity.html#a4ba5755857ef6fb19765f01798b45615", null ],
    [ "getName", "classRootImpedanceFromBulkDensity.html#afb2987027e923f336e9938597ad0b767", null ],
    [ "bulkDensity", "classRootImpedanceFromBulkDensity.html#ac71a12f5ad1d0c6ab6d149dc2369acc3", null ],
    [ "inGrowthpoint", "classRootImpedanceFromBulkDensity.html#a6e7bbc258237b3368cf0296980c559ee", null ],
    [ "pSoilWaterContent", "classRootImpedanceFromBulkDensity.html#a8cb307d804b04d97efba928d91c8c8fc", null ]
];