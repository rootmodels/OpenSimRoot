var classWaterUptakeFromHopmans =
[
    [ "WaterUptakeFromHopmans", "classWaterUptakeFromHopmans.html#a7f877be59d73103f5564a1bfd8091a10", null ],
    [ "calculate", "classWaterUptakeFromHopmans.html#a4da8d7c7c610f92ec710e3e5002ac5de", null ],
    [ "getName", "classWaterUptakeFromHopmans.html#aaeb17d6675c461135bc3cea359cac1f6", null ],
    [ "mode", "classWaterUptakeFromHopmans.html#ab270f0a15e0847542f2cba639ea99669", null ],
    [ "potentialTranspiration", "classWaterUptakeFromHopmans.html#a8e714aef5bc93d413324f79d4461d212", null ],
    [ "segmentLength", "classWaterUptakeFromHopmans.html#a6dac44432b94b6a7caecd35b81883f71", null ],
    [ "totalRootLength", "classWaterUptakeFromHopmans.html#a3a77978429175b3363bb05fe955812a7", null ]
];