var classVolumetricHeatCapacity =
[
    [ "VolumetricHeatCapacity", "classVolumetricHeatCapacity.html#aba7863638012b7f10eb784f0ad96bd4e", null ],
    [ "calculate", "classVolumetricHeatCapacity.html#a63ce0984a0bb6075a22a77a83f0b0145", null ],
    [ "getName", "classVolumetricHeatCapacity.html#a64f165962a388c568246e30200b0f3ef", null ],
    [ "theta_", "classVolumetricHeatCapacity.html#a5b59ef7d303b9d70cbdc47095029f5f6", null ],
    [ "theta_organic_matter_", "classVolumetricHeatCapacity.html#a06abcac9e8f74f0704bc1d280323e106", null ],
    [ "theta_other_minerals_", "classVolumetricHeatCapacity.html#ae662042b488502d319f3f816ae853415", null ],
    [ "theta_quartz_", "classVolumetricHeatCapacity.html#ac1a3c1028dfb6b21e72f305c77f62e15", null ],
    [ "volumetricHeatCapacity_", "classVolumetricHeatCapacity.html#a34949f4369fff427312ef53003251613", null ]
];