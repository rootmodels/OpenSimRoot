var classLateralHydraulicConductivity =
[
    [ "LateralHydraulicConductivity", "classLateralHydraulicConductivity.html#a45540c4199905193b78aa1aa83eb1afa", null ],
    [ "calculate", "classLateralHydraulicConductivity.html#acc6a128d24eb0904ba230ce571344f16", null ],
    [ "getName", "classLateralHydraulicConductivity.html#a6aeb2fe288670f51a43c5dfb590dcae7", null ],
    [ "cond", "classLateralHydraulicConductivity.html#a9ec6450f70fbf98810413a477a315e75", null ],
    [ "inverse", "classLateralHydraulicConductivity.html#a819f3773d7b9f9e27aa815b21645a179", null ],
    [ "pDiameterOfXylemVessels", "classLateralHydraulicConductivity.html#adc29cf478c252eff51f7f0a577ff7f7d", null ],
    [ "pNumberOfXylemVessels", "classLateralHydraulicConductivity.html#af4967db39882c7bf39c67ecb4fe1387c", null ],
    [ "RCSeffect", "classLateralHydraulicConductivity.html#a84b30ddb731f8d599489f2b0c100d9ca", null ],
    [ "RCSstage", "classLateralHydraulicConductivity.html#a8fc58505eb69705c6a940bc2b1e6bfe3", null ],
    [ "rootSegmentStartTime", "classLateralHydraulicConductivity.html#ab21219e27b03adc60be18951d73d62ef", null ],
    [ "size", "classLateralHydraulicConductivity.html#aea95f3c8db03d9c1f2bec7b567c8ebea", null ],
    [ "soilK", "classLateralHydraulicConductivity.html#a693a8dddc9d3fe4de4e987ad88ed1e9f", null ],
    [ "soilKconversionFactor", "classLateralHydraulicConductivity.html#a4ca3f81d9435b7971eaa5e1de7c7244b", null ]
];