var classRootSegmentRootHairSurfaceArea =
[
    [ "RootSegmentRootHairSurfaceArea", "classRootSegmentRootHairSurfaceArea.html#a4f7b8622f3ef3b08dc9eb8a35a02eb9b", null ],
    [ "calculate", "classRootSegmentRootHairSurfaceArea.html#ab3c9520efb87d912e0b9e4305ec01f80", null ],
    [ "getName", "classRootSegmentRootHairSurfaceArea.html#a33a35fcc382a5b274b6b9468257d217c", null ],
    [ "length", "classRootSegmentRootHairSurfaceArea.html#ac45b50fed8af7105042f0cf23e3a59f9", null ],
    [ "rhdensity", "classRootSegmentRootHairSurfaceArea.html#aa38ff466336aec15283f8caccdd9b83f", null ],
    [ "rhdiameter", "classRootSegmentRootHairSurfaceArea.html#a3eff58172c6694e157457d93622c26bd", null ],
    [ "rhlength", "classRootSegmentRootHairSurfaceArea.html#aece7ca6def085a3c81ae1322f0c69626", null ]
];