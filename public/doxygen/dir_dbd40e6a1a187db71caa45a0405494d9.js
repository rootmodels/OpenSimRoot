var dir_dbd40e6a1a187db71caa45a0405494d9 =
[
    [ "NutrientUptake.cpp", "NutrientUptake_8cpp.html", null ],
    [ "NutrientUptake.hpp", "NutrientUptake_8hpp.html", [
      [ "Barber_cushman_1981_nutrient_uptake_ode23", "classBarber__cushman__1981__nutrient__uptake__ode23.html", "classBarber__cushman__1981__nutrient__uptake__ode23" ],
      [ "BarberCushmanSolverOde23", "classBarberCushmanSolverOde23.html", "classBarberCushmanSolverOde23" ],
      [ "Barber_cushman_1981_nutrient_uptake_explicit", "classBarber__cushman__1981__nutrient__uptake__explicit.html", "classBarber__cushman__1981__nutrient__uptake__explicit" ],
      [ "BarberCushmanSolver", "classBarberCushmanSolver.html", "classBarberCushmanSolver" ],
      [ "Barber_cushman_1981_nutrient_uptake", "classBarber__cushman__1981__nutrient__uptake.html", "classBarber__cushman__1981__nutrient__uptake" ],
      [ "BiologicalNitrogenFixation", "classBiologicalNitrogenFixation.html", "classBiologicalNitrogenFixation" ],
      [ "Imax", "classImax.html", "classImax" ],
      [ "SegmentMaxNutrientUptakeRate", "classSegmentMaxNutrientUptakeRate.html", "classSegmentMaxNutrientUptakeRate" ],
      [ "RadiusDepletionZoneSimRoot4", "classRadiusDepletionZoneSimRoot4.html", "classRadiusDepletionZoneSimRoot4" ],
      [ "RadiusDepletionZoneBarberCushman", "classRadiusDepletionZoneBarberCushman.html", "classRadiusDepletionZoneBarberCushman" ],
      [ "RootSegmentNutrientDepletionVolume", "classRootSegmentNutrientDepletionVolume.html", "classRootSegmentNutrientDepletionVolume" ]
    ] ],
    [ "NutrientUptake2.cpp", "NutrientUptake2_8cpp.html", null ],
    [ "NutrientUptake3.cpp", "NutrientUptake3_8cpp.html", null ]
];