var classRootGrowthDirection =
[
    [ "RootGrowthDirection", "classRootGrowthDirection.html#a856688e31eefb9e25a0698cdd9b23c3a", null ],
    [ "calculate", "classRootGrowthDirection.html#a79ae525e7c11fbb7a7d3a703adfdda7f", null ],
    [ "getLateralBranchingAngle", "classRootGrowthDirection.html#a6c4083bdb5123e2ea02c1e1a3791d03b", null ],
    [ "getName", "classRootGrowthDirection.html#af72bb43d0ad6e503c41573239f9c8446", null ],
    [ "getRadialBranchingAngle", "classRootGrowthDirection.html#acc64ab44db2c8175dee442a866807996", null ],
    [ "postIntegrationCorrection", "classRootGrowthDirection.html#a63029a6c3e68954109364427fa05444f", null ],
    [ "setInitialDirection", "classRootGrowthDirection.html#a90f31ba61f25be3aae467bca7ac35772", null ],
    [ "bottomBoundary", "classRootGrowthDirection.html#a745f3e89a87b416f9bb0be109cc236fe", null ],
    [ "bounceOfSide", "classRootGrowthDirection.html#ab22453885151e7c4a590b037bd8b3beb", null ],
    [ "cannotgrowup", "classRootGrowthDirection.html#a1ce408d3adbd96ee8d67f12138e732d0", null ],
    [ "gravitropismSimulator", "classRootGrowthDirection.html#a841fc84f79cd3d2db36f7c6ad3077f9c", null ],
    [ "growthSimulator", "classRootGrowthDirection.html#a21aeeb8b346634d7e16b659bb9e1f7dc", null ],
    [ "idirection", "classRootGrowthDirection.html#a208975b291fe4e3146f69b5eed12244c", null ],
    [ "impedenceSimulator", "classRootGrowthDirection.html#a3c5b985018f6ba72ecc4b8119ea0e161", null ],
    [ "macroImpedance", "classRootGrowthDirection.html#a628d6c366b027cbfea962a8e3f63dcc8", null ],
    [ "parent", "classRootGrowthDirection.html#a9f9be379789cf870da777e3ff643d0a3", null ],
    [ "refTimeG", "classRootGrowthDirection.html#a5dfa13c83053bbd0794b162e1d6d272f", null ],
    [ "refTimeI", "classRootGrowthDirection.html#add4ba4e61f39000f5d73b16fd4fbce38", null ],
    [ "steps_", "classRootGrowthDirection.html#a0d92fcda2eea9571e9c43064d91a73b3", null ],
    [ "topBoundary", "classRootGrowthDirection.html#a38cba4dadc5c9e2990487537abdfda53", null ],
    [ "tropismSimulator", "classRootGrowthDirection.html#a02d708c09e75a751c7b12d8da936662b", null ]
];