var dir_ae9a93452e2a84339148a16bcf2eb561 =
[
    [ "BiCGSTAB.cpp", "BiCGSTAB_8cpp.html", null ],
    [ "BiCGSTAB.hpp", "BiCGSTAB_8hpp.html", [
      [ "BiCGSTAB", "classBiCGSTAB.html", null ]
    ] ],
    [ "CSRmatrix.cpp", "CSRmatrix_8cpp.html", null ],
    [ "CSRmatrix.hpp", "CSRmatrix_8hpp.html", "CSRmatrix_8hpp" ],
    [ "InterpolationLibrary.cpp", "InterpolationLibrary_8cpp.html", "InterpolationLibrary_8cpp" ],
    [ "InterpolationLibrary.hpp", "InterpolationLibrary_8hpp.html", "InterpolationLibrary_8hpp" ],
    [ "MathLibrary.cpp", "MathLibrary_8cpp.html", "MathLibrary_8cpp" ],
    [ "MathLibrary.hpp", "MathLibrary_8hpp.html", "MathLibrary_8hpp" ],
    [ "pcgSolve.cpp", "pcgSolve_8cpp.html", null ],
    [ "pcgSolve.hpp", "pcgSolve_8hpp.html", [
      [ "Pcg", "classPcg.html", null ]
    ] ],
    [ "SolverTypes.hpp", "SolverTypes_8hpp.html", "SolverTypes_8hpp" ],
    [ "SparseMatrix.cpp", "SparseMatrix_8cpp.html", null ],
    [ "SparseMatrix.hpp", "SparseMatrix_8hpp.html", "SparseMatrix_8hpp" ],
    [ "SparseSymmetricMatrix.cpp", "SparseSymmetricMatrix_8cpp.html", null ],
    [ "SparseSymmetricMatrix.hpp", "SparseSymmetricMatrix_8hpp.html", "SparseSymmetricMatrix_8hpp" ],
    [ "VectorMath.cpp", "VectorMath_8cpp.html", "VectorMath_8cpp" ],
    [ "VectorMath.hpp", "VectorMath_8hpp.html", "VectorMath_8hpp" ]
];