var classSuperCoring =
[
    [ "SuperCoring", "classSuperCoring.html#a7e88120603745a501d487872d42b7200", null ],
    [ "~SuperCoring", "classSuperCoring.html#ae1cdaa84b361027580f953aa8a2d5b22", null ],
    [ "calculate", "classSuperCoring.html#a6c7cc1770208ae471a25585a8e4a7ed5", null ],
    [ "getName", "classSuperCoring.html#ab4196de4bb7c7788a9a7d27926c0affa", null ],
    [ "center", "classSuperCoring.html#aa3a3e1a8f9f1cb7d865f470390189169", null ],
    [ "depth", "classSuperCoring.html#a103a447d3513c6e344a5edcf907ed675", null ],
    [ "label", "classSuperCoring.html#a198e6e7ccc26256bd7f834bfab3ce827", null ],
    [ "maxdia", "classSuperCoring.html#a4f00b2602a73357784ae34d78da2f5bf", null ],
    [ "maxslices", "classSuperCoring.html#a64a8daee81bcbdfa0b57422aee23b1c0", null ],
    [ "mindia", "classSuperCoring.html#a23c11d11c4fefff519d792ebd3957455", null ],
    [ "os", "classSuperCoring.html#a80597060fb11f1e5d7e09cf93933a73f", null ],
    [ "param", "classSuperCoring.html#a884701d0b6ef653709316fdb58bbf020", null ],
    [ "radius", "classSuperCoring.html#aefcbc79f4d8524ad81cf4a96db49e5bc", null ],
    [ "spacing", "classSuperCoring.html#a32fe23b17acf296a1259a228eed0a4f2", null ],
    [ "sqradius", "classSuperCoring.html#a7c2871f7191c80140adf8eb83fb4a599", null ],
    [ "threshparam", "classSuperCoring.html#a07b3b76394a4b722effd30305a2b1b95", null ]
];