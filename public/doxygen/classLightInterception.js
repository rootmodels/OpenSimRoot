var classLightInterception =
[
    [ "LightInterception", "classLightInterception.html#a062f354b4d71fb3a79fbb766ce981eca", null ],
    [ "calculate", "classLightInterception.html#a9bea1098b7f1d3c51268701602016129", null ],
    [ "getName", "classLightInterception.html#ab57538a7baa7722f26c773b6ff2ac969", null ],
    [ "irradiationSimulator", "classLightInterception.html#a923dea47be6ff7e101c6e5a4c176b076", null ],
    [ "KDF", "classLightInterception.html#a6e2d2f31b5a2c7697e1e955edd50ae3a", null ],
    [ "leafAreaIndexSimulator", "classLightInterception.html#a429f5192c973c97e25548b6ed5f2126b", null ],
    [ "RDDPAR", "classLightInterception.html#ad463c1811b8fb90eb3a44cf4a63e2970", null ]
];