var SolverTypes_8hpp =
[
    [ "Solvertype", "SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986", [
      [ "NO_PRECOND", "SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986a1f75a979690d7de61927c794e7a9cfde", null ],
      [ "GaussSeidel", "SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986aef6690918f97c4d6a495791ab249587f", null ],
      [ "ILU", "SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986a19ab38c7bc356be73943bb05827ed573", null ],
      [ "Jacobi", "SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986ad265c8b1fda63fad2de4994858396ed8", null ],
      [ "DEFAULT", "SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986a88ec7d5086d2469ba843c7fcceade8a6", null ],
      [ "JACOBI", "SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986a0565cd5c5ac848bda19ad297b242ce5e", null ],
      [ "SYM_GAUSS_SEIDEL", "SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986a1bebbb07a04c1a2d906aaf2dd3c9838e", null ],
      [ "SSOR", "SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986ad00b05f9b6dbe32d5079de216bf24b7b", null ]
    ] ]
];