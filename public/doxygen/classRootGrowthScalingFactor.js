var classRootGrowthScalingFactor =
[
    [ "RootGrowthScalingFactor", "classRootGrowthScalingFactor.html#a4bfb8dfa375f686c9db39cfc1b3f6dff", null ],
    [ "calculate", "classRootGrowthScalingFactor.html#ac159ecd8a1826e233c0983e4deaa1567", null ],
    [ "getName", "classRootGrowthScalingFactor.html#a333c04f9a7b0767206a67d0aa1fc3ad8", null ],
    [ "postIntegrationCorrection", "classRootGrowthScalingFactor.html#a11bd69fbe16df8c5bcda316912ad3a9f", null ],
    [ "c2rootsSimulator", "classRootGrowthScalingFactor.html#a16994f3943132c125638c5e6791c6cf9", null ],
    [ "c2SecondaryGrowth", "classRootGrowthScalingFactor.html#a6ec11fa6bdfcd88e2cba943bf4cb93fb", null ],
    [ "CinDryWeight", "classRootGrowthScalingFactor.html#a5d61197f6ec53190be2b10ff7dc07b18", null ],
    [ "mode", "classRootGrowthScalingFactor.html#ad5d29002dab2578a81787803e5f452d4", null ],
    [ "mt", "classRootGrowthScalingFactor.html#a6b045542290dc507f99e2a8baf01f4f7", null ],
    [ "multiplyReservesByCarbonFraction", "classRootGrowthScalingFactor.html#a683f02561e5ed9b8ca79c984ebca06ff", null ],
    [ "plantingTime", "classRootGrowthScalingFactor.html#a76dd6a732c3f11ee6dff48f404299bbc", null ],
    [ "potentialRootGrowthSimulator", "classRootGrowthScalingFactor.html#ae169f702311ba98d66f2db741f0f7bd7", null ],
    [ "potentialRootGrowthSimulatorMajorAxis", "classRootGrowthScalingFactor.html#ad00afddbef337e2675aef30894435909", null ],
    [ "rates", "classRootGrowthScalingFactor.html#adce5338064acbdb73d4c3771c9cb70d8", null ],
    [ "reserves", "classRootGrowthScalingFactor.html#a77f61192b4a5144dd188b36f6274e88d", null ],
    [ "scale_", "classRootGrowthScalingFactor.html#aecd18e03573f2eb7da08ffe5e7297ece", null ],
    [ "threshold", "classRootGrowthScalingFactor.html#af5763546338ba2cfe878c46c9fb1afbd", null ]
];