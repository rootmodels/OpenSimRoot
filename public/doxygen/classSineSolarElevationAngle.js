var classSineSolarElevationAngle =
[
    [ "SineSolarElevationAngle", "classSineSolarElevationAngle.html#acfb12b21fd593d2044cb422d5ca61253", null ],
    [ "calculate", "classSineSolarElevationAngle.html#a22dd09225ef22795b5c497b2e4b20470", null ],
    [ "getName", "classSineSolarElevationAngle.html#a4a89335829815e4a035b380b90ebde0f", null ],
    [ "cleanUpTime", "classSineSolarElevationAngle.html#afed041ece9c6e6aaaab62cda00a7f9c8", null ],
    [ "cosLatitude", "classSineSolarElevationAngle.html#aba54c72009b99cad2c70f32c6c305cf5", null ],
    [ "it1", "classSineSolarElevationAngle.html#a13469ad3e8233b31ed59a2560f481223", null ],
    [ "it2", "classSineSolarElevationAngle.html#aaaccc1421bd1ec5c998b9697dde2445f", null ],
    [ "newCleanUpTime", "classSineSolarElevationAngle.html#ab1dd0ce37864ddc98717ce571cf6c872", null ],
    [ "savedValues", "classSineSolarElevationAngle.html#ab1734ba3c3219b9b25cd734e73691da9", null ],
    [ "sinLatitude", "classSineSolarElevationAngle.html#ac0a96aff182779a10cc6e725d7bc973b", null ],
    [ "startDay", "classSineSolarElevationAngle.html#a09abf0d9d37f51f54fd749873e1304b8", null ],
    [ "startYear", "classSineSolarElevationAngle.html#ae04fe2f48c9192a259765eac632c807b", null ]
];