var classSumOverPlants =
[
    [ "SumOverPlants", "classSumOverPlants.html#adca9fd3dbcd21df450f902ffb12b2878", null ],
    [ "calculate", "classSumOverPlants.html#a2afd4d3bfa87949dd24d4005233e0914", null ],
    [ "getName", "classSumOverPlants.html#a18f83d4bf24eecde432dc87cd6047c94", null ],
    [ "container", "classSumOverPlants.html#aec14438cb16ebcf9c9297c23198bc3dc", null ],
    [ "mean", "classSumOverPlants.html#ad5053010b50a3d35794cfc32ff69ce41", null ],
    [ "n", "classSumOverPlants.html#aa68c5fb90b494599616206624532df83", null ],
    [ "name", "classSumOverPlants.html#a1a72b14693faf3245a0e7795c4e85f62", null ],
    [ "plantingTimes", "classSumOverPlants.html#a31b8249303b8cac9e2dae81a594e5998", null ],
    [ "pointers", "classSumOverPlants.html#a1632f6c2c21f5d07d12e5a903fa18ecd", null ],
    [ "rates", "classSumOverPlants.html#a22a212fade2249c8a7d334f15689a04a", null ]
];