/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "OpenSimRoot", "index.html", [
    [ "SimRoot", "index.html", null ],
    [ "Todo List", "todo.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"ANSImanipulators_8hpp.html",
"IntegrationLibrary_8cpp.html#ab9d4ac7d9f2a9b110b63311c5af11a60",
"ReadXMLfile_8hpp.html#aba9ba502b3094a1da7aaaf9bfd46baa4",
"Tag_8cpp_source.html",
"classBarberCushmanSolverOde23.html#a4a084624c4f6adc52c63227583b1ce9a",
"classCarbonAvailableForGrowth.html#a84b79ffdb90ebf80bb43d747319921ae",
"classExportBase.html#a8bce70b639924b2f148bdafff6e690f4",
"classLeafRespirationRate.html#ac1dbf26f3129504ffdf125084f4d9f56",
"classMesophyllCO2Concentration.html#a0cd3948da441d703416b6573e5053d9a",
"classPhotosynthesisLintul.html#a3f2a4312204b18b285dd27c2e2da0866",
"classRelativeCarbonAllocation2ShootPotentialGrowth.html",
"classRootNodePotentialCarbonSinkForGrowth.html#adf3d3ae94c9e6488ebea45f5eddf7a13",
"classSimplePredictor.html#a6dfaf3e8b8086a50126abc4fe44f72a4",
"classSimulaDynamic.html#a54b824e479d835272e10a4f01be4db5b",
"classSineSolarElevationAngle.html#a09abf0d9d37f51f54fd749873e1304b8",
"classStomatalConductance.html#a548ae4f4d098927aa1b83ffb814af57c",
"classTillerFormation.html#a7ded2a23e5dd9cdc80c4696efff2c580",
"classWatflow.html#a5ba865c6f699c9700ef9114d050a3f28",
"exprtk_8hpp.html#afa8de0b487b0164019ed68be58ce5e87",
"structStateRate.html#a4b1336819f022e4e9a7c154f703c22c5"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';