var classTimeUntilNextSunset =
[
    [ "TimeUntilNextSunset", "classTimeUntilNextSunset.html#a712baa807ff49826242b66f261bf9ad1", null ],
    [ "calculate", "classTimeUntilNextSunset.html#a97e2979fb8860cec7441d494be9907ad", null ],
    [ "getName", "classTimeUntilNextSunset.html#afca920a22700744851b3ac23be1cbc44", null ],
    [ "cosLatitude", "classTimeUntilNextSunset.html#a4b99c61a65573a399516544dc433c401", null ],
    [ "savedValues", "classTimeUntilNextSunset.html#afce4a810b7eb5b2d74d100e7974a1e65", null ],
    [ "sinLatitude", "classTimeUntilNextSunset.html#a957ad014ddd1e96616a6c98f95df88da", null ],
    [ "startDay", "classTimeUntilNextSunset.html#ae7760d70640d007bb9d3040fda157c57", null ],
    [ "startYear", "classTimeUntilNextSunset.html#ae2e2d54ccbaa20a7823785da1f76a5f0", null ],
    [ "storedTime", "classTimeUntilNextSunset.html#a2fc49b3cb2fba860895272263b524fd3", null ]
];