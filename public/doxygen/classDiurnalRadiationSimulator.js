var classDiurnalRadiationSimulator =
[
    [ "DiurnalRadiationSimulator", "classDiurnalRadiationSimulator.html#a4f9caea35940bd826c8a1c250b5862a8", null ],
    [ "calculate", "classDiurnalRadiationSimulator.html#ac2dbaf99f3247151dbe93e4f14deb2a5", null ],
    [ "getName", "classDiurnalRadiationSimulator.html#a65b2c0ad980396f610ef788232637bd4", null ],
    [ "cleanUpTime", "classDiurnalRadiationSimulator.html#ab492fee01df4acc6b69d48b26d84d6d7", null ],
    [ "cosLatitude", "classDiurnalRadiationSimulator.html#a05fc0a7b5affd09a674a586c1ed0766d", null ],
    [ "cosSlope", "classDiurnalRadiationSimulator.html#a12394b019dd8c95e0db72921333a77da", null ],
    [ "it1", "classDiurnalRadiationSimulator.html#a9d27aa46a7e4d638c73d722ba779f107", null ],
    [ "it2", "classDiurnalRadiationSimulator.html#a7b492fa239d1240a7a0483cf563a7222", null ],
    [ "newCleanUpTime", "classDiurnalRadiationSimulator.html#af4568a0758e346c65c9d0d280554e305", null ],
    [ "pCloudCover", "classDiurnalRadiationSimulator.html#a91b1ae14d8b77c3edfc45124f1a7efe8", null ],
    [ "pReferenceSolarRadiation", "classDiurnalRadiationSimulator.html#a5045df94de4a268643ee79797a84e9b8", null ],
    [ "savedValues", "classDiurnalRadiationSimulator.html#a8c6373813ee3a50c010acc9e20a86bf2", null ],
    [ "saz", "classDiurnalRadiationSimulator.html#a4b305d9194140362e47f061ecf02c368", null ],
    [ "sinLatitude", "classDiurnalRadiationSimulator.html#a6e210eca5ca94ff2b7ca01ff9bdfe720", null ],
    [ "sinSlope", "classDiurnalRadiationSimulator.html#a55a4fd298baecab56e7dd98edb02b2f4", null ],
    [ "slope", "classDiurnalRadiationSimulator.html#a38958adca12f9866dbcc867262138127", null ],
    [ "solarElevationAngle", "classDiurnalRadiationSimulator.html#aa9510e397ef389028570667dd1b7550e", null ],
    [ "startDay", "classDiurnalRadiationSimulator.html#af899d88a87c03c7d0079874c92225b83", null ],
    [ "startYear", "classDiurnalRadiationSimulator.html#a8462f0fbffb34d0a9b9ea98687c63b27", null ]
];