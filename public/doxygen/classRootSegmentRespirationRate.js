var classRootSegmentRespirationRate =
[
    [ "RootSegmentRespirationRate", "classRootSegmentRespirationRate.html#ab2d422e16e701976d8976d5bef4e5c04", null ],
    [ "calculate", "classRootSegmentRespirationRate.html#aaead3daf06bfa5066f22a1fd21968172", null ],
    [ "getName", "classRootSegmentRespirationRate.html#a0ee078a3147dde2b0c6cf7dd150cd4ff", null ],
    [ "aerenchymaCorrectionSimulator", "classRootSegmentRespirationRate.html#af3ffce9cd33cb38f3d7bbe5f903145eb", null ],
    [ "aerenchymaSimulator", "classRootSegmentRespirationRate.html#a197118075efe018a620aeb42cb67aec4", null ],
    [ "factor", "classRootSegmentRespirationRate.html#a7ab8c69bf9dc765c43cd8ac5afd92644", null ],
    [ "mode", "classRootSegmentRespirationRate.html#a0232b11c14cb7ec258642f271befe5ff", null ],
    [ "relativeRespirationSimulator", "classRootSegmentRespirationRate.html#a044a0005ba98d9489691d4903b83ebdc", null ],
    [ "sizeSimulator", "classRootSegmentRespirationRate.html#a35a644ebaa0df4ad3c8428690a93a2cb", null ]
];