var classSaxton =
[
    [ "Saxton", "classSaxton.html#a1554989a3f48959e1d1bddd80092acab", null ],
    [ "Saxton", "classSaxton.html#ae6f6d15ddcbff7302c64aaf35e2c5393", null ],
    [ "calculate", "classSaxton.html#a5b0c789d2ae81466000b4585b758a087", null ],
    [ "getName", "classSaxton.html#a8542ef1a26ec233ae5d50da852970195", null ],
    [ "C", "classSaxton.html#ac6bcadd9841d8d2ca99b7c3249befe59", null ],
    [ "clay", "classSaxton.html#a538c06c2c222b87032cf7b37d09f91db", null ],
    [ "densityFactor", "classSaxton.html#a105aed44e7eb60ac0bf6423a0bae7aaf", null ],
    [ "DF", "classSaxton.html#aafd784921c9261a92271dde5c9c0c6dd", null ],
    [ "gravel", "classSaxton.html#a42078c6e12a43e537df1000536649091", null ],
    [ "OM", "classSaxton.html#a1a93bae7967a8dc06672cf80a9b918fb", null ],
    [ "organic", "classSaxton.html#a1a3ab9ee63f69d8a8e38686a6596e0f6", null ],
    [ "par", "classSaxton.html#a31de6ef854d7c4eeb2b738fc088d4fd3", null ],
    [ "returnParm", "classSaxton.html#ad493cf81f9ef30f17b1e8cc8dc88e3a0", null ],
    [ "Rv", "classSaxton.html#a4e605454039cd2d7143e16f047587468", null ],
    [ "S", "classSaxton.html#ab3dccf6fe689f9c5a2d1ce7aa754c9a1", null ],
    [ "sand", "classSaxton.html#aa022ce486ddafcb5206ec03c1643ac11", null ],
    [ "waterContent", "classSaxton.html#a495b6fcbe6363f64be8e516d376fecf7", null ]
];