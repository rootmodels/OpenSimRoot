var classTillerFormation =
[
    [ "TillerFormation", "classTillerFormation.html#af996dd3b8f315b93761ff8fdfa740e8e", null ],
    [ "~TillerFormation", "classTillerFormation.html#abbd56034e469ef2d5e35ae6c4b9befd4", null ],
    [ "generate", "classTillerFormation.html#a938e2c54f396df2cdcebd2bdcd280d6e", null ],
    [ "initialize", "classTillerFormation.html#a77157a81875319462d4fae0b3bb976d4", null ],
    [ "crownDiameters", "classTillerFormation.html#aa1882eadd48a4f345cd5c169d9d2df9e", null ],
    [ "gp", "classTillerFormation.html#ae1c35c89920553a23b54e6f7072e0ec3", null ],
    [ "maxNumberOfTillers", "classTillerFormation.html#ad2ebd3adc8fb8619d50d0b8b8f7f4e93", null ],
    [ "maxTillerNumber", "classTillerFormation.html#a3e50134f9e1ce79d69033a1ff917aad0", null ],
    [ "maxTillerOrder", "classTillerFormation.html#ae4d5f8187f36fb436da99ce2e3e929a8", null ],
    [ "newPosition", "classTillerFormation.html#a0877b96614cac3f5debcdb7930b6a540", null ],
    [ "pot", "classTillerFormation.html#a77ff63bf3c9d652bcecb8f3ec26240f2", null ],
    [ "readTillerPositionsFromFile", "classTillerFormation.html#a50a81bbc45447e08eb071485c008279f", null ],
    [ "relativeTillerPositions", "classTillerFormation.html#a64f841e99aab0f19ec6f2f2284aae6ff", null ],
    [ "tillernumber", "classTillerFormation.html#a6652affa918871b35a832e1ec09cd1d9", null ],
    [ "tillerOrdering", "classTillerFormation.html#a3d34e7cb94248e87ac2a90979c32157f", null ],
    [ "tillerOrderStress", "classTillerFormation.html#a84cf1b2425dc50ef2f5a0de643617194", null ],
    [ "tillerPosDegree", "classTillerFormation.html#ae2af5936b245e1b930b643288e436b6e", null ],
    [ "tillerPositions", "classTillerFormation.html#a7910a86f91a18fd784c3efb22e1ed487", null ],
    [ "tillerPosX", "classTillerFormation.html#a9c74b260cca351f26dbac7a47ca764bd", null ],
    [ "tillerPosZ", "classTillerFormation.html#a2430aced7766fcc423c1216152ce4058", null ],
    [ "tillerStress", "classTillerFormation.html#a3af563d3b9f77d7d80a88fa5619a787f", null ],
    [ "timeDelayBetweenTillerOrders", "classTillerFormation.html#a3d341b25a20b997af645238f409dbd4b", null ],
    [ "timingOfLastTiller", "classTillerFormation.html#a9035c06a84e33412e28bf8c844c4a9d2", null ],
    [ "timingOfLastTillerOrder", "classTillerFormation.html#a927708fe6073126848e7df7acf2856be", null ],
    [ "timingOfTillers", "classTillerFormation.html#ae340d5188a601a5c02015980a2c9db8e", null ],
    [ "verticalOffsets", "classTillerFormation.html#a7ded2a23e5dd9cdc80c4696efff2c580", null ],
    [ "xs", "classTillerFormation.html#a84baa61f5c015ab2a58772b254043b0e", null ],
    [ "zs", "classTillerFormation.html#a599b7f871c92c5d51980d5db40c2023f", null ]
];