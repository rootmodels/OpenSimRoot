var classSoluteMassBalanceTest =
[
    [ "SoluteMassBalanceTest", "classSoluteMassBalanceTest.html#a6e32e1e6aeb425599f0d7ac9c4771129", null ],
    [ "calculate", "classSoluteMassBalanceTest.html#a53359c9511b40438acf956ede58d0627", null ],
    [ "getName", "classSoluteMassBalanceTest.html#a0b3dd98ab327a54e1c45dd52f42a5092", null ],
    [ "massBalanceError_", "classSoluteMassBalanceTest.html#a318adfb54d6e0b85ce041700ea4c629f", null ],
    [ "ref", "classSoluteMassBalanceTest.html#ad8eeae46638599e9df235a4118662b78", null ],
    [ "seedReserves", "classSoluteMassBalanceTest.html#a4ec56c70d249b4970e94a3cdf194094a", null ],
    [ "sumBottomBoundaryFlux_", "classSoluteMassBalanceTest.html#a991b350328f1606c87049d2c6874fcca", null ],
    [ "sumTopBoundaryFlux_", "classSoluteMassBalanceTest.html#ad40e5a911256017fb3bd73050bbdfe1d", null ],
    [ "totalMineralization_", "classSoluteMassBalanceTest.html#a8e0314cc0016acfd3b598438c82795e2", null ],
    [ "totalNutrientUptake_", "classSoluteMassBalanceTest.html#a2ffc00b9b2fa9bca376d08c4df4aa8e8", null ],
    [ "totFertilization_", "classSoluteMassBalanceTest.html#a89cb41ba1cb7d4cf3004cfe79b241e00", null ],
    [ "totSink_", "classSoluteMassBalanceTest.html#ad859455153a803e0affe00aa83cb491a", null ],
    [ "totSoluteChange", "classSoluteMassBalanceTest.html#a81b830a9bc750d95a6aa323bb9c5fae0", null ]
];