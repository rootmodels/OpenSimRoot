var classLocalNutrientResponse =
[
    [ "LocalNutrientResponse", "classLocalNutrientResponse.html#a3fd1721e23a306b95c86521558ff2799", null ],
    [ "calculate", "classLocalNutrientResponse.html#abde1cae62e05d9a76fb1ea548c2d756d", null ],
    [ "calculate", "classLocalNutrientResponse.html#a869cdefb5cd1dedfc57fce201331c9b4", null ],
    [ "getName", "classLocalNutrientResponse.html#a8d6a2cb6fb6da3d3bd096e13b2049ffb", null ],
    [ "aggregationFunction", "classLocalNutrientResponse.html#afb913ee3dc35f3d3ecf97c804b74e8b6", null ],
    [ "impactFactors", "classLocalNutrientResponse.html#a73804d19dd796dd1eb72b19ce2d08dd7", null ],
    [ "impactFactorsMod", "classLocalNutrientResponse.html#ae2db84db042103d685ff82c6d8fdbe1f", null ],
    [ "nutrients", "classLocalNutrientResponse.html#acf6594aab2d5f97e5c9fd60c7f298dc6", null ],
    [ "nutrientsMod", "classLocalNutrientResponse.html#a226d62ad9b5b45ea242e08585585e32c", null ],
    [ "position", "classLocalNutrientResponse.html#aa6cc11a841665ffbdc8df4728448f2b0", null ]
];