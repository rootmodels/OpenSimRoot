var StringExtensions_8hpp =
[
    [ "LessStringPointer", "classLessStringPointer.html", "classLessStringPointer" ],
    [ "convertFromString", "StringExtensions_8hpp.html#a4d183d43e30133964b42e92c37d7cb52", null ],
    [ "convertToString", "StringExtensions_8hpp.html#a6127243b6a18f94822ea8a9fd7511828", null ],
    [ "convertToString", "StringExtensions_8hpp.html#a04216481f01819c397e0c967351f8d3b", null ],
    [ "findWord", "StringExtensions_8hpp.html#a9c1623ced46a329b7d06a2248d27e689", null ],
    [ "ltrim", "StringExtensions_8hpp.html#ad0cb16755c59489838f05364dd080cd1", null ],
    [ "nextWord", "StringExtensions_8hpp.html#a7f265630c110e8286c30b43a1a76ae44", null ],
    [ "rtrim", "StringExtensions_8hpp.html#a141668703041152c424ec6735261fb52", null ],
    [ "string2bool", "StringExtensions_8hpp.html#abce33733e4170d648affb72f50312438", null ],
    [ "stripQuotationMarks", "StringExtensions_8hpp.html#a454d07b932f1ba371f5cbae246088d7a", null ],
    [ "trim", "StringExtensions_8hpp.html#aec6a1e2a1d381dfc46b46a3791ce6863", null ]
];