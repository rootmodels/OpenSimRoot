var classPhotoPeriod =
[
    [ "PhotoPeriod", "classPhotoPeriod.html#aea6dcf64c4bce884904fb4f48c092f64", null ],
    [ "calculate", "classPhotoPeriod.html#a4a62d882d021f4bff653ef206a51953d", null ],
    [ "getName", "classPhotoPeriod.html#a46fe6139a5e62a431a6c50afb65ffaa0", null ],
    [ "cosLatitude", "classPhotoPeriod.html#a8bb5ff8e057d1e697062a6516b02ada9", null ],
    [ "savedValues", "classPhotoPeriod.html#ad35d8e3e82a42dd90d18d14b368de78b", null ],
    [ "sinLatitude", "classPhotoPeriod.html#a81a2b5814bf644272a67179234c97095", null ],
    [ "startDay", "classPhotoPeriod.html#a9cb46b2ade794ebefa7a8c6f184d3bb6", null ],
    [ "startYear", "classPhotoPeriod.html#a74ea6479cde0ec1b4e8c11d991fea70e", null ]
];