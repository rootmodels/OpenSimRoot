var dir_72127ffe6d28be818d415c917ff61c08 =
[
    [ "AirParameter.cpp", "AirParameter_8cpp.html", null ],
    [ "AirParameter.hpp", "AirParameter_8hpp.html", [
      [ "AirDensity", "classAirDensity.html", "classAirDensity" ],
      [ "AirPressure", "classAirPressure.html", "classAirPressure" ],
      [ "SpecificHeatCapacityOfAir", "classSpecificHeatCapacityOfAir.html", "classSpecificHeatCapacityOfAir" ]
    ] ],
    [ "DiffusionResistance.cpp", "DiffusionResistance_8cpp.html", null ],
    [ "DiffusionResistance.hpp", "DiffusionResistance_8hpp.html", [
      [ "StomatalResistance", "classStomatalResistance.html", "classStomatalResistance" ],
      [ "AerodynamicResistance", "classAerodynamicResistance.html", "classAerodynamicResistance" ]
    ] ],
    [ "ETbase.cpp", "ETbase_8cpp.html", null ],
    [ "ETbase.hpp", "ETbase_8hpp.html", [
      [ "ETbaseclass", "classETbaseclass.html", "classETbaseclass" ]
    ] ],
    [ "EvapoEquations.cpp", "EvapoEquations_8cpp.html", null ],
    [ "EvapoEquations.hpp", "EvapoEquations_8hpp.html", [
      [ "PenmanMonteith", "classPenmanMonteith.html", "classPenmanMonteith" ],
      [ "Penman", "classPenman.html", "classPenman" ],
      [ "PriestleyTaylor", "classPriestleyTaylor.html", "classPriestleyTaylor" ],
      [ "Stanghellini", "classStanghellini.html", "classStanghellini" ],
      [ "Grass_reference_evapotranspiration", "classGrass__reference__evapotranspiration.html", "classGrass__reference__evapotranspiration" ],
      [ "Tall_reference_Crop", "classTall__reference__Crop.html", "classTall__reference__Crop" ]
    ] ],
    [ "Interception.cpp", "Interception_8cpp.html", null ],
    [ "Interception.hpp", "Interception_8hpp.html", [
      [ "Interception", "classInterception.html", "classInterception" ],
      [ "InterceptionV2", "classInterceptionV2.html", "classInterceptionV2" ]
    ] ],
    [ "Radiation.cpp", "Radiation_8cpp.html", "Radiation_8cpp" ],
    [ "Radiation.hpp", "Radiation_8hpp.html", [
      [ "Radiation", "classRadiation.html", "classRadiation" ],
      [ "SineSolarElevationAngle", "classSineSolarElevationAngle.html", "classSineSolarElevationAngle" ],
      [ "DiurnalRadiationSimulator", "classDiurnalRadiationSimulator.html", "classDiurnalRadiationSimulator" ],
      [ "PhotoPeriod", "classPhotoPeriod.html", "classPhotoPeriod" ],
      [ "TimeUntilNextSunset", "classTimeUntilNextSunset.html", "classTimeUntilNextSunset" ],
      [ "SoilRadiationFactor", "classSoilRadiationFactor.html", "classSoilRadiationFactor" ],
      [ "BeamRadiationSimulator", "classBeamRadiationSimulator.html", "classBeamRadiationSimulator" ],
      [ "DiffuseRadiationSimulator", "classDiffuseRadiationSimulator.html", "classDiffuseRadiationSimulator" ]
    ] ],
    [ "Transpiration.cpp", "Transpiration_8cpp.html", null ],
    [ "Transpiration.hpp", "Transpiration_8hpp.html", [
      [ "PotentialTranspirationCrop", "classPotentialTranspirationCrop.html", "classPotentialTranspirationCrop" ],
      [ "SimplePotentialTranspirationRate", "classSimplePotentialTranspirationRate.html", "classSimplePotentialTranspirationRate" ],
      [ "ActualTranspiration", "classActualTranspiration.html", "classActualTranspiration" ],
      [ "StomatalConductance", "classStomatalConductance.html", "classStomatalConductance" ],
      [ "MeanStomatalConductance", "classMeanStomatalConductance.html", "classMeanStomatalConductance" ]
    ] ],
    [ "VaporPressure.cpp", "VaporPressure_8cpp.html", "VaporPressure_8cpp" ],
    [ "VaporPressure.hpp", "VaporPressure_8hpp.html", [
      [ "ActualVaporPressure", "classActualVaporPressure.html", "classActualVaporPressure" ],
      [ "SaturatedVaporPressure", "classSaturatedVaporPressure.html", "classSaturatedVaporPressure" ],
      [ "SlopeVaporPressure", "classSlopeVaporPressure.html", "classSlopeVaporPressure" ]
    ] ]
];