var classSimulaVariable =
[
    [ "Colum1", "classSimulaVariable.html#ac164a1faac44ad8c725d3eb3e41398fc", null ],
    [ "Colum2", "classSimulaVariable.html#aeaa5ea528306543f1338304ad0fdedfb", null ],
    [ "Table", "classSimulaVariable.html#a952b4d11e879e6df5268d83f07bf97c6", null ],
    [ "Type", "classSimulaVariable.html#a62af069e9c0a0fb6141c1a5fbc60eb0f", null ],
    [ "SimulaVariable", "classSimulaVariable.html#a12c4ee4b443a2e88497dab69f8595765", null ],
    [ "SimulaVariable", "classSimulaVariable.html#aa9b78065da722a48954b33192d94d59f", null ],
    [ "~SimulaVariable", "classSimulaVariable.html#a0329e18e7f45e58168e9784b37a25241", null ],
    [ "collectGarbage", "classSimulaVariable.html#ae77df7fe5223f9b2172189488df32762", null ],
    [ "createAcopy", "classSimulaVariable.html#ac605b32c6ab5e11a7ef27c0a67ad788a", null ],
    [ "get", "classSimulaVariable.html#a9c38cf8c21d770c07cb551df3cfb425c", null ],
    [ "getRate", "classSimulaVariable.html#a6881aacfb8d2cbbc4914cc83e89c3a31", null ],
    [ "getTime", "classSimulaVariable.html#a643b3e191005545d81866472d23a986b", null ],
    [ "getType", "classSimulaVariable.html#a2ad75d481bc47d0f63531c1466aa5294", null ],
    [ "getXMLtag", "classSimulaVariable.html#a33541fe4bc4f77191b56de4cddb2cd4e", null ],
    [ "lastTimeStep", "classSimulaVariable.html#a4ebb7620e91e831d500dc3ae89cb1ba5", null ],
    [ "set", "classSimulaVariable.html#aec8b833f4661b0241cc4f5cce1ac5654", null ],
    [ "setInitialRate", "classSimulaVariable.html#a627b70271b019d49b7d5996c8ee282cc", null ],
    [ "setInitialValue", "classSimulaVariable.html#aee4580fba3d8b7934301c3ff850bc285", null ],
    [ "operator>>", "classSimulaVariable.html#a8683b75eb6357b10fc8ac007e65dad00", null ],
    [ "table", "classSimulaVariable.html#af7ff81d149c05f8b32ec7a68ced5cef9", null ]
];