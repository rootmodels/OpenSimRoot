var classRadiusDepletionZoneSimRoot4 =
[
    [ "RadiusDepletionZoneSimRoot4", "classRadiusDepletionZoneSimRoot4.html#acf58c757f211f7f3ff19b2f4ad30b71d", null ],
    [ "calculate", "classRadiusDepletionZoneSimRoot4.html#a220c4d03c9d2402cfadc442661cfe893", null ],
    [ "getName", "classRadiusDepletionZoneSimRoot4.html#a458debb87b9eb7cee8d95151d54ab140", null ],
    [ "diffusionCoefficient", "classRadiusDepletionZoneSimRoot4.html#a133dcecc8118f4db1a7dee102230efdb", null ],
    [ "rootDiameterSimulator", "classRadiusDepletionZoneSimRoot4.html#acc1ebfa77343f8b22d32208e81011e23", null ]
];