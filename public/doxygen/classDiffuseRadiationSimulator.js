var classDiffuseRadiationSimulator =
[
    [ "DiffuseRadiationSimulator", "classDiffuseRadiationSimulator.html#af3ed1db7f636ebd988a4458e3e3528aa", null ],
    [ "calculate", "classDiffuseRadiationSimulator.html#aa8b99cf2aea5bc04190264b14a274df1", null ],
    [ "getName", "classDiffuseRadiationSimulator.html#acf332116e0062cab0510400ad713535a", null ],
    [ "cleanUpTime", "classDiffuseRadiationSimulator.html#a97826d851bf1ca2cff024aca24f8956e", null ],
    [ "cosLatitude", "classDiffuseRadiationSimulator.html#ad7d83d490f190009eadba8bfb25ce0c6", null ],
    [ "cosSlope", "classDiffuseRadiationSimulator.html#a4386d5266d49ba3ad4ef7839c94acdca", null ],
    [ "it1", "classDiffuseRadiationSimulator.html#a7ed68bc7957f898d5af265a7f277c9d1", null ],
    [ "it2", "classDiffuseRadiationSimulator.html#ac8eebfe2b92a04a846a7b2e6a48bccaa", null ],
    [ "newCleanUpTime", "classDiffuseRadiationSimulator.html#a7971ca2364140e510a92a7f1cf5cc9bb", null ],
    [ "pAtmosphericPressure", "classDiffuseRadiationSimulator.html#a056addc707bd4f318d641c02593616a5", null ],
    [ "pAtmosphericTransmissionCoefficient", "classDiffuseRadiationSimulator.html#aeced7caabe23c0a6a5d2357707737b9a", null ],
    [ "pCloudCover", "classDiffuseRadiationSimulator.html#aad86bb0bac6aed9255ef74e2ce68a5a8", null ],
    [ "pReferenceSolarRadiation", "classDiffuseRadiationSimulator.html#ae0052699932678f56e3a3db3ab025810", null ],
    [ "savedValues", "classDiffuseRadiationSimulator.html#afcb7d3b1c5613be1d82f1be94989d195", null ],
    [ "saz", "classDiffuseRadiationSimulator.html#af6c33acf58bdbab0cbf6a21e80010b5b", null ],
    [ "sinLatitude", "classDiffuseRadiationSimulator.html#a5f911d88f98d9f7e793b7974e3471af6", null ],
    [ "sinSlope", "classDiffuseRadiationSimulator.html#add0a5a0246af3359d432a2f625da0bc4", null ],
    [ "slope", "classDiffuseRadiationSimulator.html#ac3998efad48b624da38b322e3b03beba", null ],
    [ "solarElevationAngle", "classDiffuseRadiationSimulator.html#aacd183a85978b2daf1f245661679366f", null ],
    [ "startDay", "classDiffuseRadiationSimulator.html#a0c44a6fa3cececf6af53b2c3c95077e3", null ],
    [ "startYear", "classDiffuseRadiationSimulator.html#aa3bbe7f19045eaf71c5846fb9610ca0b", null ]
];