var classLeafIrradiation =
[
    [ "LeafIrradiation", "classLeafIrradiation.html#aa3850e23836acff357a12137add17e47", null ],
    [ "calculate", "classLeafIrradiation.html#a63dcfb1af6ec08ee1849371f2a2fec7c", null ],
    [ "getName", "classLeafIrradiation.html#a55b21e41dad4cc3cffd0322024613424", null ],
    [ "cachedIrradiation", "classLeafIrradiation.html#a0b8ae0f7082c6daf835d79cdd44d95d9", null ],
    [ "cachedTime", "classLeafIrradiation.html#accad4bb7086d56170606d786cc12ae17", null ],
    [ "diffuseReflection", "classLeafIrradiation.html#a96b7bf9e4374cd17570eb3d839d54eb0", null ],
    [ "horizontalReflection", "classLeafIrradiation.html#ab3f92768bc2abe458192ea2602a15ac9", null ],
    [ "leafAbsorptanceTerm", "classLeafIrradiation.html#abbe67467c00d29b9bc34a1301a9c000c", null ],
    [ "pBeamIrradiation", "classLeafIrradiation.html#a0ef60bd1f6627a1b84975f29d0ebb27f", null ],
    [ "pDiffuseIrradiation", "classLeafIrradiation.html#a79df765c6c0ff860be2ce3950bff064b", null ],
    [ "pLeafAreaIndex", "classLeafIrradiation.html#a479423d72f23d0f7766fbc71ceb93e3c", null ],
    [ "pSolarElevationAngle", "classLeafIrradiation.html#a1e64ec9601cabfb1ab5510fee05f8dc0", null ]
];