var classActualVaporPressure =
[
    [ "ActualVaporPressure", "classActualVaporPressure.html#a0f87bc88b9f833ed4010942b43445047", null ],
    [ "calculate", "classActualVaporPressure.html#ab2f3293f77a86e80171b5cd7e657f717", null ],
    [ "getName", "classActualVaporPressure.html#a77a07a0ae24bef24b831568b819528b9", null ],
    [ "dailyTemperature_", "classActualVaporPressure.html#ac383032e0764bc7f523efebbcd2ad546", null ],
    [ "dewpointtemperature_", "classActualVaporPressure.html#a23e32954bdb77675e4266fc3968ee66a", null ],
    [ "maxrelativeHumidity_", "classActualVaporPressure.html#af419b96f68b08396084ca9f1d3b95dc8", null ],
    [ "maxTemperature_", "classActualVaporPressure.html#af94a028ae8656ac69547fe36a5acda29", null ],
    [ "minrelativeHumidity_", "classActualVaporPressure.html#a2c47a7e6d350b21859051ca906596f79", null ],
    [ "minTemperature_", "classActualVaporPressure.html#a80317df9e8150ab2955d672c47172ccf", null ],
    [ "relativeHumidity_", "classActualVaporPressure.html#abb7d14b6ab8552c2e5c641cd9699531f", null ]
];