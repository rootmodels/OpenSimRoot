var classRootBranches =
[
    [ "RootBranches", "classRootBranches.html#ac29b1f5992fde7304274f27766cc5990", null ],
    [ "~RootBranches", "classRootBranches.html#ab9b5d821c12b0d85743f46335b3eb2cf", null ],
    [ "checkSpatialRules", "classRootBranches.html#aef5f214dfe002ee760b6e9fee882f1af", null ],
    [ "checkTimeRules", "classRootBranches.html#acd07da7c9f423df56ca58a87f4331869", null ],
    [ "generate", "classRootBranches.html#a1e0a6bff78e949aa5ceb53a863dd8a09", null ],
    [ "generateRoots", "classRootBranches.html#a0b9228f84764867448c649b07f8c9876", null ],
    [ "generateWhorl", "classRootBranches.html#aeff260cd5b3f3e73d4cb52d74ff57c19", null ],
    [ "getSpatialShift", "classRootBranches.html#a3a73f268483ce56524305ffd1377aa68", null ],
    [ "getTimeShift", "classRootBranches.html#ace6662e59554911acd6ac806229b8a4a", null ],
    [ "initialize", "classRootBranches.html#aed532a1abc0a3dc6ad071525b6b61fcf", null ],
    [ "aboveGroundAllowed", "classRootBranches.html#a4953350692a7902f3bded6460864add7", null ],
    [ "allometricScaling", "classRootBranches.html#a281d50277afa31c51a0861867fde318e", null ],
    [ "branchCounter", "classRootBranches.html#a7ec099d0676e70331e33b19c454ee0d8", null ],
    [ "branchEmergenceDelay", "classRootBranches.html#af98c719588b322bc006494675d72f61e", null ],
    [ "branchingDelay", "classRootBranches.html#a9c833ee0d182863febfca29830ae4b86", null ],
    [ "branchingDelayModifier", "classRootBranches.html#afa77e7886b232f62a579a41cc41391ba", null ],
    [ "branchingFrequency", "classRootBranches.html#a65313600da0f4f054fd56c7d77c4e941", null ],
    [ "branchingFrequencyModifier", "classRootBranches.html#a2c3a2af1b085b4c23c4541dfd47a3db4", null ],
    [ "branchList", "classRootBranches.html#a130c4dcdd2a204579cdcd7f16fa74ab9", null ],
    [ "branchRootType", "classRootBranches.html#ab8fff6519f14df283a277f5699eac2c6", null ],
    [ "emergenceDelayForRootsInTheSameWhorl", "classRootBranches.html#aca6ba7c9cfbed695af5a0b359897785d", null ],
    [ "latestTime", "classRootBranches.html#a173aacf34e3ad61ebde4226fa7233cdc", null ],
    [ "lengthRootTip", "classRootBranches.html#a3bff655b1e5434c51725d9e8dceccd87", null ],
    [ "maximumRootAgeForBranching", "classRootBranches.html#a1098a1df743b557b1a6cea17f92c437e", null ],
    [ "maximumRootLengthForBranching", "classRootBranches.html#ad294fd70d9326cc525305802a6e103d6", null ],
    [ "maxNumberOfBranches", "classRootBranches.html#a74cff620f16c991e19a69e667fb8be04", null ],
    [ "newTime", "classRootBranches.html#abf1ad6413ac010d7738cd4e9064a5d9a", null ],
    [ "numberOfBranches", "classRootBranches.html#a47a3553075aa474392fef5929b4dc3c9", null ],
    [ "offsetLastBranch", "classRootBranches.html#a87bcffecd0f6713f5e5af18e1bd87c4a", null ],
    [ "parentGrowthpoint", "classRootBranches.html#ae14a6c1b956fb8447be21fa49ed239bb", null ],
    [ "parentRootCreationTime", "classRootBranches.html#ae3c32eb859a7e09f16b0a4901236b021", null ],
    [ "parentRootLength", "classRootBranches.html#ac215fad455d88a54b8a15499aa400da5", null ],
    [ "parentRootType", "classRootBranches.html#afb006040751b0d289c54d2f1c1bd68e2", null ],
    [ "relTimeBF", "classRootBranches.html#a0155130c42072f88e214e72b566ee10b", null ],
    [ "spatialShift", "classRootBranches.html#a25d182623d899710cf05303a9dd7b76a", null ],
    [ "timeLastBranch", "classRootBranches.html#ac72f9362cdc1902fe793e8d61ddadb7e", null ]
];