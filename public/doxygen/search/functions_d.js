var searchData=
[
  ['naturalneighbourinterpolation_3264',['naturalNeighbourInterpolation',['../classSimulaGrid.html#afd25dd8b221e7b884b8b3b1758dbbe17',1,'SimulaGrid']]],
  ['nearestneighbourinterpolation_3265',['nearestNeighbourInterpolation',['../classSimulaGrid.html#a4c7d2830cc327ecedc5a14e1c2e7992b',1,'SimulaGrid']]],
  ['net_5fstress_3266',['net_stress',['../classImpedanceGao.html#a288fbcaec31e44df2ec152ce1c0a4b19',1,'ImpedanceGao']]],
  ['neumann_5fboundary_5fcondition_3267',['neumann_boundary_condition',['../classWatflow.html#a357c3be7a582244ba7e07fd6016e5b23',1,'Watflow']]],
  ['nextword_3268',['nextWord',['../StringExtensions_8hpp.html#a7f265630c110e8286c30b43a1a76ae44',1,'nextWord(const std::string &amp;s, std::string::size_type &amp;pos, const char sep):&#160;StringExtensions.cpp'],['../StringExtensions_8cpp.html#a7f265630c110e8286c30b43a1a76ae44',1,'nextWord(const std::string &amp;s, std::string::size_type &amp;pos, const char sep):&#160;StringExtensions.cpp']]],
  ['normalize_3269',['normalize',['../structCoordinate.html#aee9594f138739f92548101bdea35d93a',1,'Coordinate']]],
  ['normalizevector_3270',['normalizeVector',['../VectorMath_8hpp.html#a37845fe0f33f8dc67ccbecc9a6834ce2',1,'normalizeVector(Coordinate &amp;vector):&#160;VectorMath.cpp'],['../VectorMath_8hpp.html#acdf2628a9613f49c5cc91bbabd0765df',1,'normalizeVector(const Coordinate &amp;base, Coordinate &amp;direction):&#160;VectorMath.cpp'],['../VectorMath_8cpp.html#a37845fe0f33f8dc67ccbecc9a6834ce2',1,'normalizeVector(Coordinate &amp;vector):&#160;VectorMath.cpp'],['../VectorMath_8cpp.html#acdf2628a9613f49c5cc91bbabd0765df',1,'normalizeVector(const Coordinate &amp;base, Coordinate &amp;direction):&#160;VectorMath.cpp']]],
  ['nounit_3271',['noUnit',['../classUnitRegistry.html#acbf671d080ee38e7a662ea054a115475',1,'UnitRegistry']]],
  ['numberofdependents_3272',['numberOfDependents',['../classSimulaTimeDriven.html#a952c20050affca4e2f933f2743724e74',1,'SimulaTimeDriven']]],
  ['numberofroots_3273',['NumberOfRoots',['../classNumberOfRoots.html#a218e082f10e318b0ffff2018ad7e317c',1,'NumberOfRoots']]],
  ['numberoftillers_3274',['NumberOfTillers',['../classNumberOfTillers.html#a987bcce288a6fc63764e1c220fe9e978',1,'NumberOfTillers']]],
  ['nutrientstressfactor_3275',['NutrientStressFactor',['../classNutrientStressFactor.html#a2dd2c8ceb2bd47057caeb4218f2b9ed5',1,'NutrientStressFactor']]],
  ['nutrientstressfactorv2_3276',['NutrientStressFactorV2',['../classNutrientStressFactorV2.html#a602b96e254b0e695a935eac2805c47a2',1,'NutrientStressFactorV2']]]
];
