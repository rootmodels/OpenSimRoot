var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefghilmnoprstuvw",
  2: "abcdefgilmnoprstuvw",
  3: "abcdefghijklmnopqrstuvw~",
  4: "abcdefghijklmnopqrstuvwxyz",
  5: "_acdeiklmnoprstu",
  6: "cgmst",
  7: "bcdgijklnstu",
  8: "ors",
  9: "abcdefgiklmnopqrstuv",
  10: "st"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Macros",
  10: "Pages"
};

