var searchData=
[
  ['q_1489',['Q',['../classWatflow.html#a8aa52ac95c1317574d5d3c109fed1753',1,'Watflow']]],
  ['q_5f_1490',['Q_',['../classThermalConductivity.html#a84d7426a3043f797ad8a9fab08737f49',1,'ThermalConductivity']]],
  ['quadraticformula_1491',['quadraticFormula',['../MathLibrary_8cpp.html#ae9169287ba71c652c0faaddefe557a3f',1,'quadraticFormula(const double &amp;a, const double &amp;b, const double &amp;c, double &amp;sol1, double &amp;sol2):&#160;MathLibrary.cpp'],['../MathLibrary_8hpp.html#ae9169287ba71c652c0faaddefe557a3f',1,'quadraticFormula(const double &amp;a, const double &amp;b, const double &amp;c, double &amp;sol1, double &amp;sol2):&#160;MathLibrary.cpp']]],
  ['quote_1492',['QUOTE',['../Stats_8cpp.html#a87978601e53a12294c82624e90c46b76',1,'QUOTE():&#160;Stats.cpp'],['../TabledOutput_8cpp.html#a87978601e53a12294c82624e90c46b76',1,'QUOTE():&#160;TabledOutput.cpp']]]
];
