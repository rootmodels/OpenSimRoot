var searchData=
[
  ['ja_4046',['ja',['../structcsr__sparse.html#a40b3d7c7c036a4623d85b705958b4678',1,'csr_sparse']]],
  ['jmaxactivationenergy_4047',['JmaxActivationEnergy',['../classLightLimitedPhotosynthesisRate.html#abf5c6f1b7cf8537693ee9a69b2a5ab36',1,'LightLimitedPhotosynthesisRate']]],
  ['jmaxdeactivationenergy_4048',['JmaxDeactivationEnergy',['../classLightLimitedPhotosynthesisRate.html#aee155aaeebb2009191226a69e3a86c2d',1,'LightLimitedPhotosynthesisRate']]],
  ['jmaxentropyterm_4049',['JmaxEntropyTerm',['../classLightLimitedPhotosynthesisRate.html#a5cb663b10905b3e5caf713002e6075ce',1,'LightLimitedPhotosynthesisRate']]],
  ['jmaxnitrogenproportionalityconstant_4050',['JmaxNitrogenProportionalityConstant',['../classLightLimitedPhotosynthesisRate.html#a4cd92a9ff24b726dcb39effbc002ca5b',1,'LightLimitedPhotosynthesisRate']]]
];
