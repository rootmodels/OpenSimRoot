var classBioporeController =
[
    [ "BioporeController", "classBioporeController.html#a0c07b91e80306d5a9e98eadc99cc1d8d", null ],
    [ "calculate", "classBioporeController.html#a326facf043042e900d44de9f7bd84ac9", null ],
    [ "getDefaultValue", "classBioporeController.html#a3fdaf1f060da6e40788b236df4df97dc", null ],
    [ "getName", "classBioporeController.html#a36d2ceb57cf18c91f9a9ac1b02fa04e4", null ],
    [ "copyFrom", "classBioporeController.html#a4988ee400f66b320463142ea371d097b", null ],
    [ "currentPoreLength", "classBioporeController.html#af4f0a40df3d3b42534c90532797aa81f", null ],
    [ "pore", "classBioporeController.html#a532cea0fa362c3e911f6c76764d6fcbf", null ],
    [ "poreLengthMax", "classBioporeController.html#a65f7f4b284e927bafef9d64b139b2819", null ],
    [ "poreLengthMin", "classBioporeController.html#a40a9f7854aaf58bd4525a73278204c12", null ],
    [ "poreProbability", "classBioporeController.html#a233df116ffb256330af6feff52918582", null ],
    [ "poreSet", "classBioporeController.html#a0c3cdbbd6d572ffe97ba35baf7a9dc66", null ],
    [ "poreStart", "classBioporeController.html#a75bf4d80ce5d7bf5e543ca1fb12a9ab5", null ],
    [ "previousChecked", "classBioporeController.html#a556a6b3e00a1d5e20202d3dc44b1920a", null ],
    [ "rootLength", "classBioporeController.html#ac983cfba6962ba3c5bd18dd5f6b6805a", null ]
];