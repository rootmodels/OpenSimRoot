var classPlantTotal =
[
    [ "PlantTotal", "classPlantTotal.html#ad7a6ef2260a547c2cfee21138b15e6bb", null ],
    [ "calculate", "classPlantTotal.html#ad09b27ef78656a49166b2465bc99105d", null ],
    [ "getName", "classPlantTotal.html#a7bdd5e5b9083ae5d648bd9d4cf6bec3d", null ],
    [ "hypocotyl", "classPlantTotal.html#a0c88cf8017d607017c1f6697784ce351", null ],
    [ "hypocotyl2", "classPlantTotal.html#aaff6c595f02e2be58910152f1bdad78d", null ],
    [ "leafs", "classPlantTotal.html#ab5d8768b6006e6cd82cffb0b52e15340", null ],
    [ "primaryRoot", "classPlantTotal.html#aa50922b54527fb68aa6511a2e4ec0eae", null ],
    [ "primaryRoot2", "classPlantTotal.html#a62fa3c36486a5f4a092a00dbd6ce003c", null ],
    [ "runmode", "classPlantTotal.html#ae1b5f3104d19eb4258edcef94bf753ac", null ],
    [ "shootBornRoots", "classPlantTotal.html#a394178b3b5d8f97254c835e68e7501ae", null ],
    [ "shootBornRoots2", "classPlantTotal.html#af8ba9403dafc027445f25e024eb1333d", null ],
    [ "stems", "classPlantTotal.html#aba94713ea855fbc75ba453c8aa529a6d", null ]
];