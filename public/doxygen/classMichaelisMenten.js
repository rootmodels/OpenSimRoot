var classMichaelisMenten =
[
    [ "MichaelisMenten", "classMichaelisMenten.html#a2d191095dc7ba334952df668688da6b4", null ],
    [ "calculate", "classMichaelisMenten.html#a1b88087fac508ea18ee82601b355a5a6", null ],
    [ "getName", "classMichaelisMenten.html#a9c02827b76da7c6bd40b3ffa13383eee", null ],
    [ "diameter", "classMichaelisMenten.html#adf9e593288b6ea2062529b5b93bdee03", null ],
    [ "length", "classMichaelisMenten.html#a96c4d9abde4bf14a49e235be55ed1079", null ],
    [ "roothairs", "classMichaelisMenten.html#aca793c30c3f8907810696559a5df626d", null ],
    [ "sCmin", "classMichaelisMenten.html#a49459a94028705919a806d368cd900b3", null ],
    [ "sImax", "classMichaelisMenten.html#a6356e8ea28dfd6ec45594131244f92f4", null ],
    [ "sKm", "classMichaelisMenten.html#a19b893464d9222dbce1b599b3ca82284", null ],
    [ "surfconc", "classMichaelisMenten.html#a3606047ca3ac10d7076c90483cebad3a", null ],
    [ "surfWaterContent", "classMichaelisMenten.html#abc49fe64fa1f5cadd89bcaa1f2062a20", null ]
];