var classLeafAreaReductionCoefficient =
[
    [ "LeafAreaReductionCoefficient", "classLeafAreaReductionCoefficient.html#ae262f1eeea2d932a8c5054a4aacaae90", null ],
    [ "calculate", "classLeafAreaReductionCoefficient.html#ac37ca807d8466c07f683ff1d13d062d7", null ],
    [ "getName", "classLeafAreaReductionCoefficient.html#a10d41fedfc1a2237166a80d49efadc2d", null ],
    [ "actual", "classLeafAreaReductionCoefficient.html#aed5a63ff352945c355262c99698509db", null ],
    [ "potential", "classLeafAreaReductionCoefficient.html#a4f0dc09ce7a1fe9a319913fd30bf6e85", null ],
    [ "recoveryRate", "classLeafAreaReductionCoefficient.html#a7d64e7c9f0f586852a2343aebf5e9463", null ]
];