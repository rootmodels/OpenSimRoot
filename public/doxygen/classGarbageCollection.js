var classGarbageCollection =
[
    [ "GarbageCollection", "classGarbageCollection.html#a9caf3e1f0c07412ce8d302c609c0108b", null ],
    [ "collectGarbage", "classGarbageCollection.html#a92294fc98ebbdea49717903e9916d451", null ],
    [ "finalize", "classGarbageCollection.html#a252d18e5234ef3d4b5d5de9e4f90e7a9", null ],
    [ "initialize", "classGarbageCollection.html#ab973ca4f977d0decbb56c35fe6148de0", null ],
    [ "run", "classGarbageCollection.html#abbe831a20126d2a48346c9ffed2d5ddf", null ],
    [ "update", "classGarbageCollection.html#ac528394a21439d1ca3e94b303960fd3e", null ],
    [ "lacktime", "classGarbageCollection.html#adb1da90f1abb519c16bccae43d8c0efb", null ],
    [ "probe", "classGarbageCollection.html#a44457ca5438698903759e3849cbd9ae8", null ]
];