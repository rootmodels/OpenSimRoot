var classScaledWaterUptake =
[
    [ "ScaledWaterUptake", "classScaledWaterUptake.html#a3e9553e108436a98ff653f17080ab314", null ],
    [ "calculate", "classScaledWaterUptake.html#afc1095b4a5a6da898dc5352421a632d5", null ],
    [ "getName", "classScaledWaterUptake.html#ac4a7b8c3567c543c224326c571e0ae79", null ],
    [ "actualTranspiration", "classScaledWaterUptake.html#a8ace512624bcde918d57967b0fa99217", null ],
    [ "fraction", "classScaledWaterUptake.html#ae59927adf1aa6eca7fe995e07bb13e15", null ],
    [ "length", "classScaledWaterUptake.html#ab17ced9e8353f4c302bcbf591ffba51f", null ],
    [ "total", "classScaledWaterUptake.html#a9e28e66f02b9374d018b267ca8e71795", null ]
];