#!/bin/bash

cd  ../../..
make nopython -j 16
mkdir -p OpenSimRoot/Release
cp nopython_build/OpenSimRoot_noPython OpenSimRoot/Release/OpenSimRoot
cd OpenSimRoot/tests/benchmarks2020

cd C1.1
qsub -N C1.1 runall.sh

cd ../C1.2
qsub -N C1.2 runall.sh

cd ../M2.1  
qsub -N M2.1 runall.sh

cd ../M2.2  
qsub -N M2.2 runall.sh

cd ../M2.2-smooth 
qsub -N M2.2.smooth runall.sh

cd ../M2.2.fine 
qsub -N M2.2.fine runall.sh

cd ../M3.1  
qsub -N M3.1 runall.sh

cd ../M3.2
qsub -N M3.2 runall.sh
cd ..

echo "Submitted the jobs"

qstat

