#!/bin/bash


# This is a sample PBS script. It will request 1 processor on 1 node
# for 4 hours.
#
#$ -M j.postma@fz-juelich.de
#
#   Request 1 processors on 1 node
#
#PBS -l nodes=1:ppn=1
#this does not work if the parallel environment is not created $ -pe mpi 1
#
#   Request x hours of walltime, 32 GB of ram
#
#PBS -l walltime=10:20:00
#$ -l h_rt=10:20:00
#
#NOTUSED  PBS -q bigmem
#NOTUSeD  $ -q bigmem
#
#   Request that regular output and terminal output go to the same file
#
#PBS -j oe
#$ -cwd
#$ -j y
#$ -o out.$JOB_ID.o
#$ -e err.$JOB_ID.e
#
#number of nodes on sge cluster
#$ -pe smp 2
#$ -R y
#
###NU$ -q BigMem.q
###NU$ -P BigMem.p
#
#$ -l h_vmem=1G
#
#   The following is the body of the script. By default,
#   PBS scripts execute in your home directory, not the
#   directory from which they were submitted. The following
#   line places you in the directory from which the job
#   was submitted.
#
if [ $PBS_O_WORKDIR ]
 then

  cd $PBS_O_WORKDIR
fi
if [ $SGE_O_WORKDIR ]
  then
    cd $SGE_O_WORKDIR
fi
#
#   Now we want to run the program "hello".  "hello" is in
#   the directory that this script is being submitted from,
#   $PBS_O_WORKDIR.
#
wd=`pwd`
#note that $JOB_ID contains the id of the running job

echo " "
echo "working dir = $wd "
echo "Job started on `hostname` at `date`"



if [ -z "$1" ] 
then
exe="../../Release/OpenSimRoot"
else
exe="$1"
fi

# Runs the C1.2 benchmark. The non-rhizo versions take 6 hours (5 for the b version)

mkdir -p C1.2a.rhizo C1.2b.rhizo C1.2a C1.2b

cd C1.2a.rhizo
../../$exe -f  ../C12a.rhizo.xml #&
PIDar=$!
cd ../C1.2b.rhizo
../../$exe -f  ../C12b.rhizo.xml 
PIDbr=$!
#cd ..

#wait $PIDar

cd ../C1.2a
../../$exe -f  ../C12a.xml #&
PIDa=$!
cd ../C1.2b
../../$exe -f  ../C12b.xml
PIDb=$!
cd ..

#wait $PIDa

cd C1.2a
cat tabled_output.tab | tail -n 2107 > tabled_output.tab.short
cd ../C1.2b
cat tabled_output.tab | tail -n 2107 > tabled_output.tab.short
cd ..

./compareResults.sh



