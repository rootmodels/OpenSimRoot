#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import *
import numpy as np
import matplotlib.pylab as plt
import sys
#import vtk
#from vtk.util.numpy_support import vtk_to_numpy

C1aData = []
with open("C1.2a/tabled_output.tab.short", "rb") as f:
	for line in f:
		line = line.decode("utf-8", "ignore").strip()
		if '"rootWaterUptake"' in line:
#		if '"rootWaterUptakeCheck"' in line:
			lineSplit = line.split()
			if lineSplit[5] == '"//plants/simpleRoot"':
				C1aData.append([float(lineSplit[1].strip()), lineSplit[3].strip()])
	f.close()
C1bData = []
with open("C1.2b/tabled_output.tab.short", "rb") as f:
	for line in f:
		line = line.decode("utf-8", "ignore").strip()
		if '"rootWaterUptake"' in line:
			lineSplit = line.split()
			if lineSplit[5] == '"//plants/simpleRoot"':
				C1bData.append([float(lineSplit[1].strip()), lineSplit[3].strip()])
	f.close()

string1 = ""
string2 = ""
for i in range(len(C1aData)):
	string1 += str(C1aData[i][0] - 8) + ";"
	string2 += C1aData[i][1] + ";"
writeString = string1[:-1] + "\r\n" + string2[:-1]

with open("results_a/OpenSimRoot.csv", "wb") as f:
	f.write(writeString.encode("utf-8"))

string1 = ""
string2 = ""
for i in range(len(C1bData)):
	string1 += str(C1bData[i][0] - 8) + ";"
	string2 += C1bData[i][1] + ";"
writeString = string1[:-1] + "\r\n" + string2[:-1]

with open("results_b/OpenSimRoot.csv", "wb") as f:
	f.write(writeString.encode("utf-8"))


