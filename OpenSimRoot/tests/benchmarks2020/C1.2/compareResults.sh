#!/bin/bash
cd C1.2a
cat tabled_output.tab | tail -n 2107 > tabled_output.tab.short
cd ../C1.2a.rhizo
cat tabled_output.tab | tail -n 2107 > tabled_output.tab.short
cd ../C1.2b
cat tabled_output.tab | tail -n 2107 > tabled_output.tab.short
cd ../C1.2b.rhizo
cat tabled_output.tab | tail -n 2107 > tabled_output.tab.short
cd ..

#./femvtu2profile.R -o C1.2a_OSR_fig2.csv  C1.2a
#./femvtu2profile.R -o C1.2a_OSR_rhizo_fig2.csv  C1.2a.rhizo
#./femvtu2profile.R -o C1.2b_OSR_fig2.csv  C1.2b
#./femvtu2profile.R -o C1.2b_OSR_rhizo_fig2.csv  C1.2b.rhizo

./ProcessOutput.py
./ProcessOutputRhizo.py

./plotResults.py

