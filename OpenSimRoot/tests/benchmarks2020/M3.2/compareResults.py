#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import *
import numpy as np
import matplotlib.pylab as plt
import sys
import vtk
from vtk.util.numpy_support import vtk_to_numpy

from scipy.interpolate import interp1d
import os as os
#from rsml import * 
#from van_genuchten import *

col=np.array([[0.12156863,0.46666667,0.70588235,1.        ],
 [1.        , 0.49803922, 0.05490196, 1.        ],
 [0.17254902, 0.62745098, 0.17254902, 1.        ],
 [0.58039216, 0.40392157, 0.74117647, 1.        ],
 [0.54901961, 0.3372549 , 0.29411765, 1.        ],
 [0.89019608, 0.46666667, 0.76078431, 1.        ],
 [0.49803922, 0.49803922, 0.49803922, 1.        ],
 [0.7372549 , 0.74117647, 0.13333333, 1.        ],
 [0.09019608, 0.74509804, 0.81176471, 1.        ],
 [0.09019608, 0.74509804, 0.81176471, 1.        ],
 [0.09019608, 0.74509804, 0.81176471, 1.        ],
 [0.09019608, 0.74509804, 0.81176471, 1.        ],
 [0.09019608, 0.74509804, 0.81176471, 1.        ],
 [0.09019608, 0.74509804, 0.81176471, 1.        ],
 [0.09019608, 0.74509804, 0.81176471, 1.        ]])

#read osr data
reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName("3.2a/roots014.00.vtp")
reader.Update()
data = reader.GetOutput()
osr_coarse_psix = vtk_to_numpy(data.GetPointData().GetArray("xylemWaterPotential"))[0:-1]
osr_coarse_x = vtk_to_numpy(data.GetPointData().GetArray("nextxyz"))[0:-1,0]
osr_coarse_y = vtk_to_numpy(data.GetPointData().GetArray("nextxyz"))[0:-1,2]
osr_coarse_z = vtk_to_numpy(data.GetPointData().GetArray("nextxyz"))[0:-1,1]

writeString = ""
for i in range(len(osr_coarse_psix)):
	writeString += str(osr_coarse_z[i]) + ","
writeString = writeString[:-1] + "\n"
for i in range(len(osr_coarse_psix)):
	writeString += str(osr_coarse_psix[i]) + ","
writeString = writeString[:-1]
with open("results_a/3.2aOpenSimRoot", "wb") as f:
	f.write(writeString.encode('utf-8'))
	f.close()

#read osr data
reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName("3.2b/roots014.00.vtp")
reader.Update()
data = reader.GetOutput()
osr_coarse_psix = vtk_to_numpy(data.GetPointData().GetArray("xylemWaterPotential"))[0:-1]
osr_coarse_x = vtk_to_numpy(data.GetPointData().GetArray("nextxyz"))[0:-1,0]
osr_coarse_y = vtk_to_numpy(data.GetPointData().GetArray("nextxyz"))[0:-1,2]
osr_coarse_z = vtk_to_numpy(data.GetPointData().GetArray("nextxyz"))[0:-1,1]

writeString = ""
for i in range(len(osr_coarse_psix)):
	writeString += str(osr_coarse_z[i]) + ","
writeString = writeString[:-1] + "\n"
for i in range(len(osr_coarse_psix)):
	writeString += str(osr_coarse_psix[i]) + ","
writeString = writeString[:-1]
with open("results_b/3.2bOpenSimRoot", "wb") as f:
	f.write(writeString.encode('utf-8'))
	f.close()
	

def plot_results(path, xmin, xmax, ymin, ymax):

    fig, ax = plt.subplots(1, 1, figsize=(7,7)) 
    ax = [ax]
    l = ["Reference"]
    
    data = np.loadtxt(path+l[0],delimiter=',') # analytical solution
    print(data.shape)
    z_a = data[0,:]
    psi_a = data[1,:]
    ax[0].scatter(psi_a, z_a, marker ='*', color = 'red', s=1.5)
    # ax[0].plot(psi_a, z_a, 'r*')
    
    cc = 0
    nrsmea, namea = [] ,[]
    for dirname, dirnames, filenames in os.walk(path+'.'):
        filenames.sort(key=str.lower)
        for i,f in enumerate(filenames):
            try:
                if f!="Reference":
                    l.append(f)
                    data = np.loadtxt(path+f,delimiter=',')  
                    print(str(i+1)+". "+f+":from ", min(data[1,:]), "to", max(data[1,data[1,:]<-1]), " cm pressure head\n") 
                    ax[0].scatter(data[1,data[1,:]<-1], data[0,data[1,:]<-1], color = col[cc,:], s = 1.5)
                    z_n = data[0,:]
                    psi_n = data[1,:]
                    interp = interp1d(z_n, psi_n,  kind='linear', fill_value='extrapolate', bounds_error = False) 
                    psi_n = interp(z_a)                 
                    #nrsmea.append(nRMSE(psi_a,psi_n))
                    namea.append(f)
                    cc += 1
            except Exception as ex:
                print("Something went wrong with file "+f)    
                raise            
    ax[0].set_xlabel("root water pressure head (cm)")  
    # ax[0].set_xlim(xmin, xmax)
    ax[0].set_ylabel("depth (cm)")  
    # ax[0].set_ylim(ymin, ymax)
    ax[0].legend(l, loc = 3, markerscale=5)    
    plt.savefig(path.lower()[:-1]+".png", dpi=300, bbox_inches = "tight")
    #plt.show() 
    return nrsmea, namea

nrsmea, names = plot_results('results_a/', -350, -230, -19, - 2.5)
nrsmeb, names = plot_results('results_b/', -300, -190, -19, 0)



