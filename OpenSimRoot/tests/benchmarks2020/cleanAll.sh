#!/bin/bash

#cd  ../../StaticBuild/
#make all -j 16
#mkdir -p ../Release
#cp OpenSimRoot ../Release
#cd ../tests/benchmarks2020

cd C1.1
echo cleaning C1.1
./cleanAll.sh

cd ../C1.2
echo cleaning C1.1
./cleanAll.sh

cd ../M2.1  
echo cleaning M2.1
./cleanAll.sh

cd ../M2.2  
echo cleaning M2.2
./cleanAll.sh

cd ../M2.2-smooth 
echo cleaning M2.2-smooth
./cleanAll.sh

cd ../M2.2.fine 
echo cleaning M2.2.fine
./cleanAll.sh

cd ../M3.1  
echo cleaning M3.1
./cleanAll.sh

cd ../M3.2
echo cleaning M3.2
./cleanAll.sh
cd ..

echo "Finished"

qstat

