#!/bin/bash

# This is a sample PBS script. It will request 1 processor on 1 node
# for 4 hours.
#
#$ -M j.postma@fz-juelich.de
#
#   Request 1 processors on 1 node
#
#PBS -l nodes=1:ppn=1
#this does not work if the parallel environment is not created $ -pe mpi 1
#
#   Request x hours of walltime, 32 GB of ram
#
#PBS -l walltime=10:20:00
#$ -l h_rt=10:20:00
#
#NOTUSED  PBS -q bigmem
#NOTUSeD  $ -q bigmem
#
#   Request that regular output and terminal output go to the same file
#
#PBS -j oe
#$ -cwd
#$ -j y
#$ -o out.$JOB_ID.o
#$ -e err.$JOB_ID.e
#
#number of nodes on sge cluster
#$ -pe smp 1
#$ -R y
#
###NU$ -q BigMem.q
###NU$ -P BigMem.p
#
#$ -l h_vmem=1G
#
#   The following is the body of the script. By default,
#   PBS scripts execute in your home directory, not the
#   directory from which they were submitted. The following
#   line places you in the directory from which the job
#   was submitted.
#
if [ $PBS_O_WORKDIR ]
 then

  cd $PBS_O_WORKDIR
fi
if [ $SGE_O_WORKDIR ]
  then
    cd $SGE_O_WORKDIR
fi
#
#   Now we want to run the program "hello".  "hello" is in
#   the directory that this script is being submitted from,
#   $PBS_O_WORKDIR.
#
wd=`pwd`
#note that $JOB_ID contains the id of the running job

echo " "
echo "working dir = $wd "
echo "Job started on `hostname` at `date`"




# this runs the infiltration benchmark on a fine grid. 
# computational time roughly: clay (1.5 hour), loam1 (1 h), loam2 (1 h), sand (1 h). 


if [ -z "$1" ] 
then
exe="../../Release/OpenSimRoot"
else
exe="$1"
fi

mkdir -p clay 
mkdir -p loam1
mkdir -p loam2  
mkdir -p sand

cd clay
../../$exe -f ../swmsNoRoot.clay.xml  >> screen.out 2>&1 || echo "Benchmark M2.2 clay  failed"   && echo "Benchmark M2.2 clay ran successfully"  
cd ../loam1
../../$exe -f ../swmsNoRoot.loam1.xml >> screen.out 2>&1 || echo "Benchmark M2.2 loam1  failed" && echo "Benchmark M2.2 loam1 ran successfully"  
cd ../loam2
../../$exe -f ../swmsNoRoot.loam2.xml >> screen.out 2>&1 || echo "Benchmark M2.2 loam2  failed" && echo "Benchmark M2.2 loam2 ran successfully"  
cd ../sand
../../$exe -f ../swmsNoRoot.sand.xml  >> screen.out 2>&1 || echo "Benchmark M2.2 sand  failed" && echo "Benchmark M2.2 clay ran successfully"  
cd ..



# todo run test

# todo clean up
./compareResults.R


echo " "
echo "Job Ended at `date`"
echo " "

