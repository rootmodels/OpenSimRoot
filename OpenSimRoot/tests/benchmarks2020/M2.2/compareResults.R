#!/usr/bin/Rscript

#function to read one tabled_output.tab and extract info

getData<-function(filename="clay/tabled_output.tab", parametername="topBoundaryFluxRate"){
  d<-read.table(filename,header=T)
  srow= d$name==parametername
  ds=subset(d,srow)
  return(ds)
}

writeDataOfFile<-function(filename="clay/tabled_output.tab", newFile=T, s=-1.){
  d=getData(filename)
  write.table(t(d$time),file="OSRM22.txt",quote = F,row.names=F, col.names = F, sep=',', append=!newFile)
  write.table(t(s*d$value),file="OSRM22.txt",quote = F,row.names=F, col.names = F, sep=',', append=T)
}

filenames=c(
  "clay/tabled_output.tab",
  "loam1/tabled_output.tab",
  "loam2/tabled_output.tab",
  "sand/tabled_output.tab"
)
scaleFactors=c(-100.,-100.,-100.,-400.)
newFile=T
i=1
for( n in filenames){
  print(paste(n,newFile))
  s=scaleFactors[i]
  writeDataOfFile(n,newFile,s)
  newFile=F
  i=i+1
}

# plot
#reference
svg("M2.2_evaporation_comparison.svg",height=8,width=12)
refData=read.csv("analyticSolutionM2.2.csv",header=T)
plot(1e8,1e8,xlab="time (days)",ylab="E (cm/day)", xlim=c(0,6), ylim=c(0.,0.3))
points(refData$time,refData$clay,col="grey")
points(refData$time,refData$loam1,col="pink")
points(refData$time,refData$loam2,col="lightgreen")
points(refData$time,refData$sand,col="lightblue")

simData=read.csv("OSRM22.txt",header=F)
lines(simData[1,],simData[2,],col=1)
lines(simData[3,],simData[4,],col=2)
lines(simData[5,],simData[6,],col=3)
lines(simData[7,],simData[8,],col=4)

simData=read.csv("OSRM22.smooth.txt",header=F)
#low resolution
# published results
lines(simData[1,],simData[2,],col=1,lty=2,lwd=2)
lines(simData[3,],simData[4,],col=2,lty=2,lwd=2)
lines(simData[5,],simData[6,],col=3,lty=2,lwd=2)
lines(simData[7,],simData[8,],col=4,lty=2,lwd=2)

#lines(simData[9,],simData[10,],col=4,lty=2,lwd=2)
#lines(simData[11,],simData[12,],col=2,lty=2,lwd=2)
#lines(simData[13,],simData[14,],col=3,lty=2,lwd=2)
#lines(simData[15,],simData[16,],col=1,lty=2,lwd=2)


legend("topright",c("clay","loam1","loam2","sand"),col=1:4, pch=1,lty=1, bty='n')
legend(3.,0.3,c("analytic","dirichlet switch (slower, up to 1.5 hours)","smooth transition (faster, up to 2.5 min)"),col=1, pch=c(1,NA,NA),lty=c(NA,1,2), bty='n')


