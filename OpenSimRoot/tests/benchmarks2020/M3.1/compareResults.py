#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import *
import numpy as np
import matplotlib.pylab as plt

import vtk
from vtk.util.numpy_support import vtk_to_numpy


#read osr data
reader = vtk.vtkXMLPolyDataReader()
#reader.SetFileName("/home/jouke/sciebo/projects/benchmarksSchnepffEtc/v2/roots028.00.vtp")
reader.SetFileName("runFolder/roots028.00.vtp")
reader.Update()
data = reader.GetOutput()
osr_fine_psix = vtk_to_numpy(data.GetPointData().GetArray("xylemWaterPotential"))[0:-1]
osr_fine_z = vtk_to_numpy(data.GetPointData().GetArray("nextxyz"))[0:-1,1]

#write file
np.savetxt('OpenSimRootFine', (osr_fine_z,osr_fine_psix), delimiter=',')
osr_bench=np.loadtxt('OpenSimRootFine.submitted', delimiter=',')


#analytical solution
g = 1000. #9.81 # gravitational acceleration (m/s^2)   
rho =  1.e3 # 997 density of water, (kg/m^3)      
ref = 0.#1.e5 # reference pressure (kg/ (m s^2))

def toPa(ph): # cm pressure head to Pascal (kg/ (m s^2))
    return ref + ph / 100. * rho * g

def toHead(pa): # Pascal (kg/ (m s^2)) to cm pressure head
    return (pa-ref) * 100 / rho / g

# Parameters
L = 50            # length of single straight root [cm]
a = 0.2           # root radius  [cm]
kx = 0.0432       # root axial conductivity [cm^3 / day] 
kr = 1.73e-4      # root radial conductivity [1 / day]
p_s = -200        # soil matric potiential [cm]
p0 = -1000        # dirichlet bc at root collar [cm]

#radial conduct in simroot is in cm/day/hPa = 100/(60*60*24*100)= 1/1.1574074074074073e-05 m/s/pa
# m/s/pa = m/s/(kg/m/s2) = m2 s / kg 
#so kr in simroot is 2e-9 / 1.1574074074074073e-05 = 0.0001728 cm/day/hPa

#and kz is in simroot in cm4/day/hpa which is 1e-4/(60*60*24*100)= 1/1.157e-11 m4/s/pa
# m4/s/pa = m4/s/(kg/m/s2) = m5 s / kg
#so kz is 5e-13 m5 s / kg = 5e-13 / 1.157e-11 = 4.32e-2  cm4/day/hpa

#rho*g = 9.81e3 kg/m2/s2 = 9.81e3 pa / m = 0.981 hpa/cm or 1. 

# Analytical solution
c = 2*a*np.pi*kr/kx
#this a function
p_r = lambda z: p_s + d[0]*np.exp(np.sqrt(c)*z) + d[1]*np.exp(-np.sqrt(c)*z) # Eqn 6
#reduces to
# pr = 0.010193679918450561 * (-19620 + -7.84545526e+04*exp(7.089815403622064* z) + -2.54474113e+01 *exp(-7.089815403622064* z))
#t_r = lambda z: 0.010193679918450561 * (-19620 + -7.84545526e+04*exp(7.089815403622064* z) + -2.54474113e+01 *exp(-7.089815403622064* z))


# Boundary conditions
AA = np.array([[1,1], [np.sqrt(c)*np.exp(np.sqrt(c)*(-L)), -np.sqrt(c)*np.exp(-np.sqrt(c)*(-L))] ]) 
#bb = np.array([p0-p_s, -rho*0.]) #
bb = np.array([p0-p_s, -1]) 
# Eqn 10
d = np.linalg.solve(AA, bb) # compute constants d_1 and d_2 from bc

# Plot results
fig=plt.figure(figsize=(7, 7))
za_ = np.linspace(0,-L,100)
pr = list(map(p_r, za_))
plt.plot(osr_bench[1,:],osr_bench[0,:],"r*")
plt.plot(osr_fine_psix,osr_fine_z,"g*")
plt.plot(pr,za_)
plt.xlabel("Xylem pressure (cm)")
plt.ylabel("Depth (cm)")
#plt.show()
plt.savefig('comparison_M3.1.png')


