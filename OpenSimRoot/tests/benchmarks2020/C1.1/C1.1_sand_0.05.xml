<!-- Ernst Schafer, University of Nottingham, 2020 -->
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="XML/treeview.xsl"?>
<SimulationModel
	defaultStartingTimeStep="1.e-5"
	synchronizationTimeStep="1.e-5" 
	defaultMaximumTimeStep="1.e-5"
	defaultMinimumTimeStep="1.e-9"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	 xsi:noNamespaceSchemaLocation="https://rootmodels.gitlab.io/opensimroot/XML/SimulaXMLSchema.xsd">
	<SimulaBase name="environment">
		<SimulaBase name="dimensions">
		    <!--  benchmark goes to 0.6  -->
			<SimulaConstant name="minCorner" type="coordinate">-0.57 -1. -0.57</SimulaConstant><!-- one note extra as otherwise the tip is in an area of 0 conductivity. -->
			<SimulaConstant name="maxCorner" type="coordinate">0.57 0. 0.57</SimulaConstant>
			<SimulaConstant name="roundPot" type="bool">1</SimulaConstant>
			<!--  needs to be same as r_root = 0.02  in x and z, but y can be larger-->
			<!-- radius roo is 0.02, to get points position there it needs to be divided by sqrt of 2, result is 0.01414 -->
			<SimulaConstant name="resolution" type="coordinate">0.03 0.1 0.03</SimulaConstant>
		</SimulaBase>
	</SimulaBase>
	<SimulaBase name="soil"></SimulaBase>
	<SimulaBase name="plants" >
		<SimulaBase name="simpleRoot" objectGenerator="seedling">
			<SimulaConstant name="plantType" type="string">straightRoot</SimulaConstant>
			<SimulaConstant name="plantingTime" unit="day" type="time">0</SimulaConstant> 
			<SimulaConstant name="plantPosition" type="coordinate">0. 0. 0.</SimulaConstant>
		</SimulaBase>
	</SimulaBase>
	<SimulaBase name="simulationControls">
		<SimulaConstant type="integer" name="SimulaStochastic::numberOfSamples">1</SimulaConstant>
		<SimulaBase name="integrationParameters">
		    <!--  idealy same as z resolution of fem grid? or alternatively much smaller-->
			<SimulaConstant name="defaultSpatialIntegrationLength" unit="cm">0.01</SimulaConstant>
		</SimulaBase>
		<SimulaBase name="outputParameters">
			<SimulaBase name="defaults">
				<SimulaConstant name="startTime" type="time">0.</SimulaConstant>
				<!--  note that this is immediately stressed and at the moment there is only a solution compared for day 0.  -->
				<SimulaConstant name="endTime" type="time">0.0088 </SimulaConstant> <!-- we reach stress at this time. -->
				<SimulaConstant name="timeInterval" type="time">0.0001</SimulaConstant>
			</SimulaBase>
			<SimulaBase name="VTU">
				<SimulaConstant name="startTime" type="time">0.006 </SimulaConstant> 
				<SimulaConstant name="run" type="bool">1.</SimulaConstant>
				<SimulaConstant name="timeInterval" type="time">0.0001</SimulaConstant>
				<SimulaConstant name="includeShoots" type="bool">0</SimulaConstant>
				<SimulaConstant name="includeRoots" type="bool">1</SimulaConstant>
				<SimulaConstant name="includeVTUForDepletionZones" type="bool">1</SimulaConstant>
				<SimulaConstant name="includePointData" type="bool">1</SimulaConstant>
			</SimulaBase>
			<SimulaBase name="table">
				<SimulaConstant name="run" type="bool">1</SimulaConstant>
				<SimulaConstant name="searchingDepth" type="integer">50</SimulaConstant>
				<SimulaConstant name="skipTheseVariables" type="string">primaryRoot, hypocotyl, formula</SimulaConstant>
			</SimulaBase>
		</SimulaBase>
	</SimulaBase>
	<SimulaBase name="plantTemplate"></SimulaBase>
	<SimulaBase name="shootTemplate">
		<SimulaLink name="extinctionCoefficient"/>
	</SimulaBase>
	<SimulaBase name="hypocotylTemplate">
		<SimulaBase name="dataPoints" objectGenerator="rootDataPoints"></SimulaBase>
		<SimulaPoint name="growthpoint" function="rootGrowthDirection" garbageCollectionOff="true">0 0 0
			<SimulaVariable name="rootPotentialLongitudinalGrowth" unit="cm" function="potentialRootGrowthRate"/>
			<SimulaVariable name="rootLongitudinalGrowth" unit="cm" function="scaledRootGrowthRate" garbageCollectionOff="true"/>
		</SimulaPoint>
		<SimulaBase name="branches"></SimulaBase>
	</SimulaBase>
	<SimulaBase name="siblingRootTemplate">
		<SimulaBase name="dataPoints" objectGenerator="rootDataPoints"></SimulaBase>
		<SimulaPoint name="growthpoint" function="rootGrowthDirection" garbageCollectionOff="true">0 0 0
			<SimulaVariable name="rootPotentialLongitudinalGrowth" unit="cm" function="potentialRootGrowthRate"/>
			<SimulaVariable name="rootLongitudinalGrowth" unit="cm" function="scaledRootGrowthRate" garbageCollectionOff="true"/>
		</SimulaPoint>
		<SimulaBase name="branches"></SimulaBase>
	</SimulaBase>
	<SimulaBase name="dataPointTemplate">
	</SimulaBase>
	<SimulaDirective path="plantTemplate">
		<SimulaDerivative name="rootLength" unit="cm" function="plantTotalRootFraction"/>
		<SimulaDerivative name="rootVolume" unit="cm3" function="plantTotalRootFraction"/>
		<SimulaDerivative name="rootSurfaceArea" unit="cm2" function="plantTotalRootFraction"/>
		<SimulaVariable name="rootLongitudinalGrowth" unit="cm" function="plantTotalRootFraction"/>
	</SimulaDirective>
	<SimulaDirective path="shootTemplate"></SimulaDirective>
	<SimulaDirective path="hypocotylTemplate">
		<SimulaDirective path="growthpoint">
			<SimulaDerivative name="rootDiameter" unit="cm" function="rootDiameter.v2"/>
			<SimulaDerivative name="rootCircumference" unit="cm" function="rootCircumference"/>
			<SimulaConstant name="rootSegmentLength" unit="cm"/>
			<SimulaConstant name="rootSegmentVolume" unit="cm3"/>
			<SimulaConstant name="rootSegmentSurfaceArea" unit="cm2"/>
			<SimulaConstant name="rootSegmentLengthDuration" unit="cm.day"/>
			<SimulaConstant name="rootSegmentAge" unit="day"/>
			<SimulaDerivative name="rootPotentialSecondaryGrowth" unit="cm" function="rootDiameter.v2"/>
		</SimulaDirective>
		<SimulaDerivative name="rootLength" unit="cm" function="rootTotal.v2"/>
		<SimulaDerivative name="rootSystemLength" unit="cm" function="rootSystemTotal"/>
		<SimulaDerivative name="rootSurfaceArea" unit="cm2" function="rootTotal.v2"/>
		<SimulaDerivative name="rootSystemSurfaceArea" unit="cm2" function="rootSystemTotal"/>
		<SimulaDerivative name="rootVolume" unit="cm3" function="rootTotal.v2"/>
		<SimulaDerivative name="rootSystemVolume" unit="cm3" function="rootSystemTotal"/>
		<SimulaVariable name="rootSystemLongitudinalGrowth" unit="cm" function="rootSystemTotal"/>
	</SimulaDirective>
	<SimulaDirective path="siblingRootTemplate">
		<SimulaDirective path="growthpoint">
			<SimulaDerivative name="rootDiameter" unit="cm" function="rootDiameter.v2"/>
			<SimulaDerivative name="rootCircumference" unit="cm" function="rootCircumference"/>
			<SimulaConstant name="rootSegmentLength" unit="cm"/>
			<SimulaConstant name="rootSegmentVolume" unit="cm3"/>
			<SimulaConstant name="rootSegmentSurfaceArea" unit="cm2"/>
			<SimulaConstant name="rootSegmentLengthDuration" unit="cm.day"/>
			<SimulaConstant name="rootSegmentAge" unit="day"/>
			<SimulaDerivative name="rootPotentialSecondaryGrowth" unit="cm" function="rootDiameter.v2"/>
		</SimulaDirective>
		<SimulaDerivative name="rootLength" unit="cm" function="rootTotal.v2"/>
		<SimulaDerivative name="rootSystemLength" unit="cm" function="rootSystemTotal"/>
		<SimulaDerivative name="rootSurfaceArea" unit="cm2" function="rootTotal.v2"/>
		<SimulaDerivative name="rootSystemSurfaceArea" unit="cm2" function="rootSystemTotal"/>
		<SimulaDerivative name="rootVolume" unit="cm3" function="rootTotal.v2"/>
		<SimulaDerivative name="rootSystemVolume" unit="cm3" function="rootSystemTotal"/>
		<SimulaVariable name="rootSystemLongitudinalGrowth" unit="cm" function="rootSystemTotal"/>
	</SimulaDirective>
	<SimulaDirective path="dataPointTemplate">
		<SimulaVariable name="rootDiameter" unit="cm" function="secondaryGrowth"/>
		<SimulaDerivative name="rootCircumference" unit="cm" function="rootCircumference"/>
		<SimulaDerivative name="rootLength2Base" unit="cm" function="rootLength2Base"/>
		<SimulaDerivative name="rootSegmentLength" unit="cm" function="rootSegmentLength"/>
		<SimulaDerivative name="rootSegmentAge" unit="day" function="rootSegmentAge"/>
		<SimulaVariable name="rootSegmentLengthDuration" unit="cm.day" function="rootSegmentLength"/>
		<SimulaDerivative name="rootSegmentSurfaceArea" unit="cm2" function="rootSegmentSurfaceArea"/>
		<SimulaDerivative name="rootSegmentVolume" unit="cm3" function="rootSegmentVolume"/>
		<SimulaVariable name="rootPotentialSecondaryGrowth" unit="cm" function="potentialSecondaryGrowth"/>
	</SimulaDirective>
	<SimulaDirective path="soil" >
		<SimulaExternal function="Swms3d" name="Swms3d" unit="noUnit"/>
		<SimulaBase name="water">
		</SimulaBase>
	</SimulaDirective>
	<SimulaDirective path="plantTemplate">
		<SimulaVariable name="rootWaterUptake" unit="cm3" function="plantTotalRatesRootFraction"/>
	</SimulaDirective>
	<SimulaDirective path="hypocotylTemplate">
		<SimulaDirective path="growthpoint">
			<SimulaConstant name="rootSegmentWaterUptake" unit="cm3">0</SimulaConstant>
			<SimulaTable name_column1="time" name_column2="hydraulicHeadAtRootSurface" unit_column1="day" unit_column2="cm" function="getValuesFromSWMS"></SimulaTable>
		</SimulaDirective>
		<SimulaVariable name="rootWaterUptake" unit="cm3" function="rootTotalRates"/>
		<SimulaVariable name="rootSystemWaterUptake" unit="cm3" function="rootSystemTotalRates"/>
	</SimulaDirective>
	<SimulaDirective path="siblingRootTemplate">
		<SimulaDirective path="growthpoint">
			<SimulaConstant name="rootSegmentWaterUptake" unit="cm3">0</SimulaConstant>
			<SimulaConstant name="rootSegmentPotentialWaterUptake" unit="cm3">0</SimulaConstant>
			<SimulaTable name_column1="time" name_column2="hydraulicHeadAtRootSurface" unit_column1="day" unit_column2="cm" function="getValuesFromSWMS"></SimulaTable>
		</SimulaDirective>
		<SimulaVariable name="rootWaterUptake" unit="cm3" function="rootTotalRates"/>
		<SimulaVariable name="rootSystemWaterUptake" unit="cm3" function="rootSystemTotalRates"/>
	</SimulaDirective>
	<SimulaDirective path="dataPointTemplate">
		<SimulaVariable name="rootSegmentWaterUptake" unit="cm3" function="useDerivative"/>
		<!--SimulaConstant name="rootSegmentWaterUptakeRate" unit="cm3/day">0.000001</SimulaConstant-->
		<SimulaDerivative name="rootSegmentWaterUptakeRate" unit="cm3/day" function="useFormula">
		
			<!--SimulaConstant name="formula" type="string"> rootSegmentSurfaceArea*0.1 </SimulaConstant-->
			<SimulaConstant name="formula" type="string"> if( (t>0.006)  ,  rootSegmentSurfaceArea*0.05 , 0 ) </SimulaConstant>
			<SimulaBase name="variablePaths">
				<SimulaConstant name="h" type="string"> hydraulicHeadAtRootSurface </SimulaConstant>
			</SimulaBase>
		
		</SimulaDerivative>
		<SimulaTable name_column1="time" name_column2="hydraulicHeadAtRootSurface" unit_column1="day" unit_column2="cm" function="getValuesFromSWMS"></SimulaTable>
	</SimulaDirective>
	<SimulaDirective path="environment">
		<SimulaBase name="soil">
			<SimulaTable name_column1="depth" unit_column1="cm" name_column2="bulkDensity" unit_column2="g/cm3">-100 1.24 100 1.24</SimulaTable>
		</SimulaBase>
	</SimulaDirective>
	<SimulaDirective path="environment/soil">
		<SimulaBase name="water">
			<SimulaConstant name="rootSinkSmoothingDistance" unit="cm"> 0.1 </SimulaConstant> <!-- max radius for smoothing, default is 0.51*diagonal of one fem cube -->
			<SimulaConstant name="rootSinkSmoothingExponent" unit="noUnit"> 3. </SimulaConstant> <!-- larger, less smoothing, default is 1.5. Should be larger or equal to 0.. ). 0., all nodes in radius rootSinkSmoothing distrance get same sink value.  -->
			<!--SimulaTable name_column2="depthOfWaterTable" name_column1="time" unit_column1="day" unit_column2="cm">0 -200 30 -200</SimulaTable-->
		<!--
sand = Parameters(0.045, 0.43, 0.15, 3, 1000,0.5) 
loam = Parameters(0.08, 0.43, 0.04, 1.6, 50,0.5) 
clay = Parameters(0.1, 0.4, 0.01, 1.1, 10,0.5)

note here there is a parameter l=0.5.  
l  is lambda and is 0.5 in osr by default

-->
<!-- sand-->
			<SimulaTable name_column2="initialHydraulicHead" name_column1="depth" unit_column1="cm" unit_column2="cm">0 -100 -100 -100</SimulaTable>
			<SimulaTable name_column2="residualWaterContent" name_column1="depth" unit_column1="cm" unit_column2="100%">0 0.045 -300 0.045</SimulaTable>
			<SimulaTable name_column2="saturatedWaterContent" name_column1="depth" unit_column1="cm" unit_column2="100%" interpolationMethod="linear">0 0.43 -100 0.43</SimulaTable>
			<SimulaTable name_column2="vanGenuchten:alpha" name_column1="depth" unit_column1="cm" unit_column2="1/cm">0 0.15 -300 0.15</SimulaTable>
			<SimulaTable name_column2="vanGenuchten:n" name_column1="depth" unit_column1="cm" unit_column2="noUnit">0 3. -300 3.</SimulaTable>
			<SimulaTable name_column2="saturatedConductivity" name_column1="depth" unit_column1="cm" unit_column2="cm/day" interpolationMethod="linear">0 0. -0.051 0. -0.052 1000 -300 1000</SimulaTable>
			
			<!-- clay
			<SimulaTable name_column2="initialHydraulicHead" name_column1="depth" unit_column1="cm" unit_column2="cm">10 -100 -10 -100</SimulaTable>
			<SimulaTable name_column2="residualWaterContent" name_column1="depth" unit_column1="cm" unit_column2="100%">10 0.1 -10 0.1</SimulaTable>
			<SimulaTable name_column2="saturatedWaterContent" name_column1="depth" unit_column1="cm" unit_column2="100%" interpolationMethod="linear">10 0.4 -10 0.4</SimulaTable>
			<SimulaTable name_column2="vanGenuchten:alpha" name_column1="depth" unit_column1="cm" unit_column2="1/cm">10 0.01 -10 0.01</SimulaTable>
			<SimulaTable name_column2="vanGenuchten:n" name_column1="depth" unit_column1="cm" unit_column2="noUnit">10 1.1 -10 1.1</SimulaTable>
			<SimulaTable name_column2="saturatedConductivity" name_column1="depth" unit_column1="cm" unit_column2="cm/day" interpolationMethod="linear">10 0.5 -10 0.5</SimulaTable>
			-->
		</SimulaBase>
	</SimulaDirective>
	<SimulaDirective path="environment">
		<SimulaBase name="atmosphere">
			<SimulaTable name_column2="precipitation" name_column1="time" unit_column1="day" unit_column2="cm/day">0 0. 80 0.</SimulaTable>
			<SimulaTable name_column2="evaporation" name_column1="time" unit_column1="day" unit_column2="cm/day" interpolationMethod="step">0 0. 100 0.</SimulaTable>
		</SimulaBase>
	</SimulaDirective>


<!-- only to include the balance -->
	<SimulaDirective path="plants">
		<SimulaDerivative name="rootWaterUptake" function="sumOverAllPlants" unit="cm3"/>
	</SimulaDirective>
	<SimulaDirective path="/soil/water">
			<SimulaTable
				name_column1="time"
				unit_column1="day"
				name_column2="totalWaterInColumn"
				unit_column2="cm3"
				function="getValuesFromSWMS" />
			<SimulaTable
				name_column1="time"
				unit_column1="day"
				name_column2="totalWaterChangeInColumn"
				unit_column2="cm3"
				function="getValuesFromSWMS" />
			<SimulaTable
				name_column1="time"
				unit_column1="day"
				name_column2="topBoundaryFluxRate"
				unit_column2="cm3/day"
				function="getValuesFromSWMS" />
			<SimulaTable
				name_column1="time"
				unit_column1="day"
				name_column2="bottomBoundaryFluxRate"
				unit_column2="cm3/day"
				function="getValuesFromSWMS" />
			<SimulaVariable
				name="topBoundaryFlux"
				unit="cm3"
				maxTimeStep="0.01"
				function="usePath">
				0 0
				<SimulaConstant
					name="path"
					type="string">topBoundaryFluxRate
				</SimulaConstant>
			</SimulaVariable>
			<SimulaVariable
				name="bottomBoundaryFlux"
				unit="cm3"
				maxTimeStep="0.00001"
				function="usePath">
				0 0
				<SimulaConstant
					name="path"
					type="string">bottomBoundaryFluxRate
				</SimulaConstant>
			</SimulaVariable>
			<SimulaTable
				name_column1="time"
				unit_column1="day"
				name_column2="totalSinkRate"
				unit_column2="cm3/day"
				function="getValuesFromSWMS" />
			<SimulaVariable
				name="totalSink"
				unit="cm3"
				maxTimeStep="0.00001"
				function="usePath">
				0 0
				<SimulaConstant
					name="path"
					type="string">totalSinkRate
				</SimulaConstant>
			</SimulaVariable>
			<SimulaDerivative
				name="massBalanceError"
				unit="cm3"
				function="waterMassBalanceTest" />
			<SimulaTable
				name_column1="time"
				unit_column1="day"
				name_column2="relativeMassBalanceError"
				unit_column2="100%"
				function="usePath">
				0 0
				<SimulaConstant
					name="path"
					type="string"> massBalanceError
				</SimulaConstant>
			</SimulaTable>


	</SimulaDirective>
 <!-- balance till here -->



	<SimulaBase name="rootTypeParameters">
		<SimulaBase name="straightRoot">
			<SimulaBase name="resources"></SimulaBase>
			<SimulaBase name="shoot">
			</SimulaBase>
			<SimulaBase name="hypocotyl">
				<SimulaBase name="branchList"></SimulaBase>
				<SimulaConstant name="density" unit="g/cm3">0.1</SimulaConstant>
				<SimulaTable name_column1="days after germination" unit_column1="day" name_column2="growthRate" unit_column2="cm/day">0. 0. 100. 0.</SimulaTable>
				<SimulaConstant name="diameter" unit="cm">0.04</SimulaConstant>
				<SimulaConstant name="branchingAngle" unit="degrees">0</SimulaConstant>
				<SimulaConstant name="numberOfXylemPoles" type="integer">4</SimulaConstant>
				<SimulaConstant name="initialAbsoluteDirection" type="coordinate">0 1 0</SimulaConstant>
			</SimulaBase>
			<SimulaBase name="primaryRoot">
				<SimulaBase name="branchList"></SimulaBase>
				<SimulaConstant name="density" unit="g/cm3">0.1</SimulaConstant>
				<SimulaTable name_column1="days after germination" unit_column1="day" name_column2="growthRate" unit_column2="cm/day" interpolationMethod="linear">0.  200. 0.004  200. 0.006 0. 100000 0.</SimulaTable>
				<SimulaConstant name="diameter" unit="cm">0.04</SimulaConstant>
				<SimulaConstant name="branchingAngle" unit="degrees">0</SimulaConstant>
				<SimulaConstant name="numberOfXylemPoles" type="integer">4</SimulaConstant>
				<SimulaConstant name="initialAbsoluteDirection" type="coordinate">0 -1 0</SimulaConstant>
			</SimulaBase>
		</SimulaBase>
	</SimulaBase>
</SimulationModel>
