#!/bin/bash

# This is a sample PBS script. It will request 1 processor on 1 node
# for 4 hours.
#
#$ -M j.postma@fz-juelich.de
#
#   Request 1 processors on 1 node
#
#PBS -l nodes=1:ppn=1
#this does not work if the parallel environment is not created $ -pe mpi 1
#
#   Request x hours of walltime, 32 GB of ram
#
#PBS -l walltime=24:00:00
#$ -l h_rt=24:00:00
#
#NOTUSED  PBS -q bigmem
#NOTUSeD  $ -q bigmem
#
#   Request that regular output and terminal output go to the same file
#
#PBS -j oe
#$ -cwd
#$ -j y
#$ -o out.$JOB_ID.o
#$ -e err.$JOB_ID.e
#
#number of nodes on sge cluster
#$ -pe smp 6
#$ -R y
#
###NU$ -q BigMem.q
###NU$ -P BigMem.p
#
#$ -l h_vmem=1G
#
#   The following is the body of the script. By default,
#   PBS scripts execute in your home directory, not the
#   directory from which they were submitted. The following
#   line places you in the directory from which the job
#   was submitted.
#
if [ $PBS_O_WORKDIR ]
 then

  cd $PBS_O_WORKDIR
fi
if [ $SGE_O_WORKDIR ]
  then
    cd $SGE_O_WORKDIR
fi
#
#   Now we want to run the program "hello".  "hello" is in
#   the directory that this script is being submitted from,
#   $PBS_O_WORKDIR.
#
wd=`pwd`
#note that $JOB_ID contains the id of the running job

echo " "
echo "working dir = $wd "
echo "Job started on `hostname` at `date`"


# Runs the C1.1 benchmark. Clay 0.05 takes 15 hours to run! The others in total 17 hours

#echo "WARNING THIS SHOULD RUN WITHOUT GRAVITY ENABLED IN THE EXECUTABLE"

if [ -z "$1" ] 
then
exe="../../Release/OpenSimRoot"
else
exe="$1"
fi

mkdir -p sand_0.10 sand_0.05 loam_0.10 loam_0.05 clay_0.10 clay_0.05



cd sand_0.10
../../$exe -f  ../C1.1_sand_0.10.xml >> screen.out 2>&1 || echo "Benchmark C1.1 sand_0.10  failed"   && echo "Benchmark C1.1 sand_0.10 ran successfully"  &
PIDa=$!

cd ../sand_0.05
../../$exe -f  ../C1.1_sand_0.05.xml >> screen.out 2>&1 || echo "Benchmark C1.1 sand_0.05  failed"   && echo "Benchmark C1.1 sand_0.05 ran successfully"  &
PIDb=$!

cd ../loam_0.10
../../$exe -f  ../C1.1_loam_0.10.xml >> screen.out 2>&1 || echo "Benchmark C1.1 loam_0.10  failed"   && echo "Benchmark C1.1 loam_0.10 ran successfully"  &
PIDc=$!

cd ../loam_0.05
../../$exe -f  ../C1.1_loam_0.05.xml >> screen.out 2>&1 || echo "Benchmark C1.1 loam_0.05  failed"   && echo "Benchmark C1.1 loam_0.05 ran successfully"  &
PIDd=$!

cd ../clay_0.10
../../$exe -f  ../C1.1_clay_0.10.xml >> screen.out 2>&1 || echo "Benchmark C1.1 clay_0.10  failed"   && echo "Benchmark C1.1 clay_0.10 ran successfully"  &
PIDe=$!

cd ../clay_0.05
../../$exe -f  ../C1.1_clay_0.05.xml >> screen.out 2>&1 || echo "Benchmark C1.1 clay_0.05  failed"   && echo "Benchmark C1.1 clay_0.05 ran successfully"  &
PIDf=$!
cd ..

wait $PIDa
wait $PIDb
wait $PIDc
wait $PIDd
wait $PIDe
wait $PIDf


./getDataFromFEMVTU_sand_0.05.R
./getDataFromFEMVTU_sand_0.10.R
./getDataFromFEMVTU_loam_0.05.R
./getDataFromFEMVTU_loam_0.10.R
./getDataFromFEMVTU_clay_0.05.R
./getDataFromFEMVTU_clay_0.10.R

# todo run test
./collectResults.sh
./compareResults.py

# todo clean up



echo " "
echo "Job Ended at `date`"
echo " "

