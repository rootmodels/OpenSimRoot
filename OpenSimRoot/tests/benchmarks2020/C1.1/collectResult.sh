#!/bin/bash


cd sand_0.10
#../OpenSimRoot ../C1.1_sand_0.10.xml &

cd ../sand_0.05
#../OpenSimRoot ../C1.1_sand_0.05.xml &

cd ../loam_0.10
#../OpenSimRoot ../C1.1_loam_0.10.xml &

cd ../loam_0.05
#../OpenSimRoot ../C1.1_loam_0.05.xml &

cd ../clay_0.10
#../OpenSimRoot ../C1.1_clay_0.10.xml &

cd ../clay_0.05
#../OpenSimRoot ../C1.1_clay_0.05.xml
cd ..

./getDataFromFEMVTU_sand_0.05.R
./getDataFromFEMVTU_sand_0.10.R
./getDataFromFEMVTU_loam_0.05.R
./getDataFromFEMVTU_loam_0.10.R
./getDataFromFEMVTU_clay_0.05.R
./getDataFromFEMVTU_clay_0.10.R



cat sand_0.10/fem000.0074.vtu.profile.txt loam_0.10/fem011.2000.vtu.profile.txt clay_0.10/fem009.3000.vtu.profile.txt sand_0.05/fem000.0088.vtu.profile.txt loam_0.05/fem023.2000.vtu.profile.txt clay_0.05/fem018.9000.vtu.profile.txt > OpenSimRoot_tab.txt

cat sand_0.10/fem000.0074.vtu.profile.txt loam_0.10/fem010.0000.vtu.profile.txt clay_0.10/fem008.5000.vtu.profile.txt sand_0.05/fem000.0088.vtu.profile.txt loam_0.05/fem021.2000.vtu.profile.txt clay_0.05/fem017.5000.vtu.profile.txt > OpenSimRoot_tab_early.txt

cat OpenSimRoot_tab.txt | tr ' ' ',' > results/cur
cat OpenSimRoot_tab_early.txt | tr ' ' ',' > results/early

rm OpenSimRoot_tab.txt OpenSimRoot_tab_early.txt

echo "generating figure"
./compareResults.py

