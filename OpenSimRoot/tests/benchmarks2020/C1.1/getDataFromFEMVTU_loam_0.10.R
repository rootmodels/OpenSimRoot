#!/usr/bin/Rscript
folder="loam_0.10"
files=dir(folder,pattern = "^fem.........vtu$")
#files=c("fem001.00.vtu","fem003.00.vtu","fem005.00.vtu")

rootPos=c(0,-0.5, 0) # 1 cm root going from 0 to -1
dr=0.1
dataarrayname="hydraulic_head"

source("getDataFromFEMVTU_functions.R")

